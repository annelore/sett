# sett-gui smoke tests
This document lists the manual tests that should be run for `sett-gui` before and after a new
release of the tool is made.  
Ideally, the above test should be run on all of the following operating systems:
* GNU-Linux
* Windows 10
* MacOS

In addition of the tests listed below, any new feature or bug fix that is specific to the new
release should also be tested.  
**Note:** obviously, only dummy data should be used in these tests.


## 1. Encryption test
Encrypt at least 1 file and 1 directory for 2 or more recipients. The tester's own key should be
included in the recipient list so he/she can then test to decrypt the output.

Testing steps to perform in the **Encrypt** tab:  
* **Add files**, to add one or more files to encrypt. Include at least one file > 100 MB.
* **Add directory**, to add one or more directories to encrypt.
* **Recipients**, add 2 or more recipients (including your own key to be able to use the package in the decryption process) with the **+** button.
* Enter a **DTR ID** to which you will test transfer the encrypted data (requests Internet access and a _valid_ **DTR ID**).
  Uncheck **Verify DTR ID** if you do NOT want the **DTR ID** being verified.
* **Output suffix**, set to `test` or anything you like.
* **Change location**, change output file location to a custom location.
* **Test**, to test run.
* **Package & Encrypt**, to run the full encryption workflow. If you specified a **DTR ID**, then the file generated should 
look like `<project-code>_20210920T181602_<suffix>.zip`.

## 2. Decryption test
Testing steps to perform in the **Decrypt** tab:  
* **Add files**, to select the file you encrypted at step 1.
* Select **Decrypt selected files**. Tick/untick **Decompress** checkbox.
* **Change location**, to set the decrypted file location to a custom location.
* **Test**, to test run. You should get an error if you specified a suffix during encryption.
* **Decrypt selected files**, to run the full encryption workflow.
* Verify that the data has been properly decrypted and unpacked to the selected location.

## 3. File transfer test
Testing steps to perform in the **Transfer** tab:  
* **Add files**, to select the file you encrypted at step 1.
* Fill the SFTP **Protocol** connection details for either:
    * Your local BioMedIT node:
        * **User name**: your user name
        * **Host URL**: node landing zone URL
        * **Destination directory**: `upload` (_leomed_ and _sensa_) or `uploads` (_sciCORE_).
    * A local test SFTP server (for setup, see instructions at the end of this document):
        * **User name**: `sftp`
        * **Host URL**: `localhost` or `localhost:2222` if port 22 is mapped to port 2222 (e.g.
          when using podman).
        * **Destination directory**: `upload`
* **Test**, to test run.
* **Transfer selected files**, to transfer the test file.
* Verify that the file was correctly transferred by either:
    * connecting to your BioMedIT project's project space. E.g. via the guacamole remote desktop
      instance of your project.
    * If you have used a local test SFTP server, login to check that the files have been properly
      transferred with: `sftp sftp@localhost` or `sftp -P 2222 sftp@localhost`.

**Note:** make sure to login with your VPN so that your IP address is whitelisted of your
local node's firewall.

## 4. Key management test

### 4.1 Generate a new PGP key pair and upload it to the DCC keyserver
Testing steps to perform in the **Keys** tab. Note that you can use any user name or email when
generating a new test key, but it should contain the exact string `testkey` (case insensitive)
in either the full name or the email, e.g. `Bob Testkey` or `bob.test@testkey.com`.
But `Bob Testkey2` is not acceptable. Keys containing the exact string `testkey` are automatically deleted
from the keyserver every evening.
* **Generate a new key pair** with the following values:
    * Full name: _Bob Testkey_
    * institution/project: _SIB_
    * Institutional email: _bob.testkey@example.com_
* Verify the key is now listed under **Private keys** and **Public keys**.
* **Upload Keys**, after having selected the new public key. This uploads it to the DCC keyserver.
* To verify that key has been uploaded, press **Download keys** and search for `Bob Testkey`.

Note: the newly created key pair can be deleted locally with the following `gpg` command:
```
# UNIX:
TMP_KEY=$(gpg --list-key bob.testkey@example.com | grep -E "^[[:space:]]*[0-9A-F]{40}$") && gpg --batch --yes --delete-secret-keys $TMP_KEY && gpg --batch --yes --delete-keys $TMP_KEY && unset TMP_KEY

# Windows:
gpg --delete-secret-keys bob.testkey@example.com
gpg --delete-keys bob.testkey@example.com
```
After having delete the key with the command above, try pressing **Refresh keys** in the sett-gui
interface. The test key should disappear from the key lists.

### 4.2 Download key from keyserver
Testing steps to perform in the **Keys** tab:
* **Download keys**, and search for "alice.smith". This should download Alice's test key to
  your local machine (fingerprint: `AD1008B5F0F876630B9AF245AA2BF155EF144550`)
* Verify that Alice's key is now listed under **Public keys**.
* Verify that Alice's key has the mention **This key has been verified**.
* **Update keys**, after having selected Alice's public key. This will not change the key, but
  you should see a message saying the update was successful.
* **Delete keys**, after having selected Alice's public key.
* Verify that Alice's key has now been removed from the **Public keys** list.


## Deploying a local SFTP test server for file transfers
If you do not have access to a BioMedIT node, or do not wish to transfer files out of your local
machine, you can test transfer files using a SFTP test server docker container.

Full instructions on how to deploy the container can be found at:
https://git.dcc.sib.swiss/biwg/container/sftp_test_server


##### Command reminder when building from scratch
The example below uses `podman-compose`, but `docker-compose` can be used just as well.
```
# Clone repo:
git clone git@git.dcc.sib.swiss:biwg/container/sftp_test_server.git
cd sftp_test_server

# Start SFTP server and test access:
podman-compose up -d
sftp -P 2222 -i ./sftp_server/sftp_ssh_testkey sftp@localhost

# Stop and delete container when you are done:
podman-compose down
```

##### Command reminder when pulling from container registry
```
# Login to container registry (only needed if container has to be pulled):
docker login registry.dcc.sib.swiss

#  Pull and run container, copy its private SSH key and test access:
docker run -d -p 22:22 --name sftp_test registry.dcc.sib.swiss/biwg/container/sftp_test_server:latest
docker cp sftp_test:/home/sftp/.ssh/sftp_ssh_testkey .
sftp -i ./sftp_ssh_testkey sftp@localhost

# Stop and delete container:
docker stop sftp_test
docker rm sftp_test
```
