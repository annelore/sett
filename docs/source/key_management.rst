.. _section-pgp-key-management:

PGP key management with sett
============================

Introduction to public-key cryptography, PGP and GnuPG
------------------------------------------------------

Public-key cryptography is a method for secure communication between two or more users. In this
system, each user has a pair of unique keys consisting of a **private key** and a **public key**.
Public and private keys are linked in the sense that, whatever has been encrypted with a given
public key can only be decrypted with the matching private key, and whatever has been signed with
a given private key will only be recognized by the matching public key. Because these keys are
based on the `OpenPGP <https://www.openpgp.org/>`_ protocol, they will here be referred to as
**PGP keys**.

Public and private PGP keys:

* **Public keys** are used to encrypt data, as well as for verifying signatures made on files
  or emails. By design, public keys are intended to be shared with other people and therefore
  no particular effort is required to keep them secret.
  In fact, public keys are often uploaded to public servers, known as *keyservers*, where they
  are accessible to anyone. No password is required to use a public key.

  Typically, public keys are used by data senders to encrypt data for one or more recipient(s),
  and by data recipients to verify signatures of files or emails (to ensure the sender is genuine).

* **Private keys**, sometimes also referred to as a **secret keys**, are used to decrypt data,
  sign files and sign other people's public keys. To increase security, private keys should be
  password protected.

  .. important::

      **Private keys and their password must be kept secret at all times. Never share your
      private key or password with anyone.**

      The private key should be stored in a directory only accessible by the key owner, and
      the private key's password should be stored in a password manager.

  Typically, private keys are used by data recipients to decrypt data, and by data senders to
  sign the files they encrypt.

  .. important::

      If anyone else than the legitimate owner has access to a private key and/or its password,
      **the key pair is considered compromised**.
      It :ref:`must be revoked<Revocation certificates>`, and never used again.

  .. warning:: **GPG security warning**

     The GnuPG software keeps key passwords entered by the users temporarily stored for the
     duration of a user's session - i.e. until the user logs out of a session. This is done for
     convenience reasons, so that users only need to enter their private key password once per
     session.

     A security implication is that if someone has physical access to your computer, that person
     can now use your private key without having to know its password.

*sett* uses the open source implementation of public-key cryptography provided by the GNU Privacy
Guard software (**GPG** or **GnuPG**, in short). GnuPG is a command line tool developed by the Free
Software Foundation that uses the **PGP** `OpenPGP <https://www.openpgp.org/>`_ implementation to
encrypt and decrypt files.
A detailed documentation of PGP and GnuPG can be found in this online
`manual <https://www.gnupg.org/gph/en/manual.html>`_.

It is also possible - and often desirable - to both encrypt and sign a file. This ensures that the
data can only be read by the intended recipient, and that the recipient can be confident the sender
is legitimate. This is precisely what *sett* does:

* **Encrypting files**, so that only the intended recipient(s) can read them.
* **Signing files**, so that the recipient(s) can trust the sender is genuine.


Key fingerprints
^^^^^^^^^^^^^^^^

Each pair of public/private keys is identified by a unique **fingerprint**. Fingerprints are 40
characters long hexadecimal strings (digits and upper case A-F letters) that look like this::

    238565936FCFF3F200219990941A3EC20555F781

Since nothing is preventing two PGP keys to have the same user name and email address, it is
critical that users **always verify the genuineness of new keys** before (or just after)
importing them into their local keyring (i.e. their local database).

Ensuring a key is genuine can be done in two different ways:

* Ask the key owner to **provide their key's fingerprint via a trusted communication
  channel** (e.g. over the phone), and then verify that the fingerprint of the newly
  imported key does match the fingerprint.

* Look for a :ref:`key signatures<Public key signing>` of a **trusted key** on the newly
  imported key (e.g. the key of another person or organization that is trusted).

File encryption
^^^^^^^^^^^^^^^

In public key cryptography, the sender **encrypts** a file using one or more recipient(s) public
key(s).
Once a file is encrypted, no one can read the file without having access to the private key(s)
that matches the public key(s) used for encryption. This ensures that only the intended recipient(s)
can decrypt the file, because they are the only one to have access to the matching private key.

File signing
^^^^^^^^^^^^

The objective of **file signing** is to guarantee to the recipient of a file (or email) that the
sender is genuine, and not someone else trying to impersonate the sender.

To achieve this, the sender signs the file with their private key (password required), and shares
their public key with the recipient (typically via a keyserver). The recipient can then validate
the authenticity of the signature using the public key of the sender.
Since public keys are non-sensitive, they can be distributed publicly. In fact they are intended
for this purpose, hence their name.

Public key signing
^^^^^^^^^^^^^^^^^^

Public key **signing**, often also referred to as **certification** is intended to increase the
level of trust that people can put into someone’s public key.

By signing the public key of user "A", user "B" vouches for the genuineness of user "A"'s public
key. This allows people that already trust user "B" to increasing their level of trust in the
public key of user "A", even if they don't know user "A" directly.

Public keys can be signed/certified by other people's private keys, and/or by a central authority.

.. admonition:: BioMedIT

    **When using sett to transfer files into the BioMedIT network, all sender and recipient keys
    must be signed by the SPHN Data Coordination Centre.** Public keys that do not carry the DCC
    signature should be considered non-trustworthy and discarded.

Revocation certificates
^^^^^^^^^^^^^^^^^^^^^^^

In the unfortunate event that a user either i) forgets their private key's password or ii) have
their private key and password stolen/compromised, they will need a way to let other people know
that their public key should no longer be trusted.

This is because:

* The key owner won’t be able to decrypt data anymore (if the password was forgotten).
* Someone else might be able to decrypt data encrypted with the public key! (if the private
  key was compromised).
* Someone else might be able to illegitimately sign files (if the private key was compromised).

This situation is what **revocation certificates** are for: by applying a revocation certificate to
a public key, and then sharing the revoked key with others (e.g. via a keyserver), the key owner
signals that their key is now "revoked" and should no longer be trusted nor used. After a key
has been revoked, it can no longer be used to encrypt/decrypt data with *sett*.

Revocation certificates can be generated at anytime from the private key, but the best practice is
to generate them directly after a new key pair is created. This ensures that the revocation
certificate will be available even if the private key or its password is lost.

.. note::

    *sett-gui* will automatically generate a revocation certificate each time a new key is created.

Since anyone with access to a revocation certificate will be able to revoke the associated key,
revocation certificates must be stored securely - e.g. in a password manager - and should never
be shared with anyone.

.. important::

    * Revocation certificates **must be stored in a safe location** (e.g. a password manager) and
      **never shared with anyone**.
    * It is best to generate a revocation certificate immediately after a new public/private key
      pair is created.
    * The actual procedure to revoke a key is detailed in the
      :ref:`Revoking PGP keys<Revoking PGP keys>` section.

Exchanging public keys via a keyserver
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Encrypting files for a specific recipient requires to have the recipient's public key on one's
local **keyring** (a keyring is a local database containing PGP keys). Similarly, verifying a
signature on a file or a public key requires to have the signee's public key available in one's
local keyring.

Public keys are not sensitive data, and therefore can in principle be sent unencrypted via email.
However, when having frequent key exchanges between multiple actors, sending public PGP keys around
by email quickly becomes cumbersome. A solution to this problem is using a so called **keyserver**
to share public keys. Keyservers are public or private servers whose sole purpose is to store
public PGP keys and allow users to search for them. In addition, public keyservers form a network
and are regularly synchronized among themselves to ensure redundancy. Private keyservers have the
same role, but do not share/synchronize the stored public keys with any other server, and possibly
have a restricted access policy so that only authorized people can search for keys.

.. admonition:: BioMedIT

    Within the BioMedIT project, all exchanges of public keys should be done via a private
    keyserver found at https://keyserver.dcc.sib.swiss.

    This keyserver can be accessed directly from the *sett* application (recommended), via a
    web browser, or through command line programs such as ``gpg``.

Public key certification status
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

By default, *sett* implements a security feature where all public keys used within *sett* must be
signed/certified by a "central authority" key.
The fingerprint of this central authority can be modified in the *sett*
:ref:`configuration file<Configuration options>`. This option can also be disabled altogether.

The certification status of a public key can be easily verified in the ``sett-gui`` application by
going to the **Keys** tab, and selecting the key in the **Public key** list:

* **If the key is certified**, a green "This key has been verified" message is displayed.

  .. image:: ../img/sett_certification_status_01.*

* **If the key is not certified**, a red "key is not authorized" warning is displayed.

  .. image:: ../img/sett_certification_status_02.*

Note that in order to be able to verify that a key has been certified, the "central authority"'s
public key must be present in your local keyring. If this is not the case, all keys will be shown
as being non-certified.
Instructions on how to download public keys are given in the
:ref:`Downloading keys from the default sett keyserver<Downloading keys from the default sett keyserver>`
section below.

.. admonition:: BioMedIT

    By default, *sett* is configured with the **BioMedIT Data Coordination Center (DCC)**'s key
    as "central authority" key. For BioMedIT users, getting their keys signed by the BioMedIT DCC is
    mandatory.

    Instructions on how to get a key signed by the DCC can be found in the
    :ref:`Requesting a signature/certification from the SPHN Data Coordination Centre<Requesting a signature/certification from the SPHN Data Coordination Centre>`
    section.



Generating a new public/private PGP key pair
--------------------------------------------

A prerequisite to encrypt and transfer files with *sett* is to have public/private key pair.

.. admonition:: BioMedIT

    **All users who intend to transfer data into the BioMedIT network must get their key signed by
    the SPHN Data Coordination Centre.** This is a verification procedure to ensure that the user
    identity indicated on the key is genuine.

.. important::

    Always keep your **private key**, its **password** and its **revocation certificate** in a
    secure location. Never share any of them with anyone.

Generating a new key pair with sett-gui
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

To generate a new public/private key pair using the *sett* graphical user interface:

#. Go to the **Keys** tab and click **Generate new private/public key**. A dialog box will
   pop-up.

   .. image:: ../img/sett_generate_key_01.*

#. Fill-in the required fields:

    * **Full name**: key owner first and last name.
    * **Institution/project**: optionally, enter the institution you are affiliated with.
    * **Institutional email**: enter the email to be associated with the key pair.
    * **Password**: the password must be at least 12 characters long and contain a mix of
      letters, numbers and special characters.

     .. warning::

         There is **no possibility to retrieve a password from a generated key**, so make
         sure to remember it and store it in a safe place (e.g. a password manager).

#. Click **Generate key** and wait for a few seconds, a new key pair will be created.

#. A new pop-up window will appear to confirm that the key was generated successfully. Click
   **Show details** to display the revocation certificate associated with the new key, as shown
   in the figure below. Copy the revocation certificate to a safe location, ideally a password
   manager, and click **OK** to close the dialog box.

   .. image:: ../img/sett_generate_key_02.*

#. The new key should now be listed in *sett* under both under **Private keys** and
   **Public keys**.

   .. image:: ../img/sett_generate_key_03.*

Generating a new key pair in command line
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The *sett* command line interface does not implement new key generation. Please use the GnuPG
command line instead::

    gpg --full-generate-key

During the key generation process, a number of questions must be answered. Here are the suggested
settings:

.. code-block:: shell

    type of key      : (1) RSA and RSA (default)
    key bit length   : 4096
    key validity time: 0 (Key does not expire)
    Real name        : your full name (e.g. Alice Smith)
    Email address:   : your institutional email address, it will be associated permanently to
                       your key.
    Comment          : optionally, a "comment" (e.g. the institution you work for) can be added
                       to the key user ID.

.. important::

    At the end of the key generation process, ``gpg`` will ask for a pass phrase (password) that
    will be associated with the private key. Please note that **if the password is forgotten, there
    is no way to retrieve or re-create it** and the private key will become unusable.

After having created a new key, it is strongly recommended to immediately generate a revocation
certificate. Note that the password of the private key must be entered in order to complete the
command.

.. code-block:: shell

    # General syntax:
    gpg --output <revocation certificate file> --gen-revoke <key fingerprint>

    # Example:
    gpg --output revocation_certificate.asc --gen-revoke AD1008B5F0F876630B9AF245AA2BF155EF144550

Listing local keys in command line
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Private and public keys available in the local keyrings can be listed with the following commands:

.. code-block:: shell

    gpg --list-keys            # list public key(s).
    gpg --list-secret-keys     # list private key(s).

Exporting/importing private keys in command line
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

In some situations (new computer setup, remote *sett* environment, ...) you might need to copy
or move your private key to a different machine. This can be done by **exporting the private key**
to a file, transferring the file to the new machine, and **importing** it.

Here is an example of how to export a private key to a file with GnuPG:

.. code-block:: shell

    # The key identifier can be its fingerprint, key ID or the user's email address.
    # NOTE: the order of arguments matters, the '--output' option must be passed first!
    gpg --output private_key.pgp --export-secret-key username@email

You will be prompted for the key password. Note that keys can also be exported in ASCII format (instead of
binary) by adding the ``--armor`` option.

After the key has been transferred to the new machine (in either binary or ASCII format),
importing it is as easy as:

.. code-block:: shell

    gpg --import private.pgp

Verify that the key has been correctly imported with ``gpg --list-secret-keys``.

**Warning:** the above method exports only a single key, not the entire content of your GnuPG
keyrings. If your intention is to create a backup of all your GPG keys, follow instructions given
`here <https://risanb.com/code/backup-restore-gpg-key/>`_ or have a look at
this `procedure <https://gist.github.com/garetmckinley/29cab22251d1913eee9556ff5b286052>`_ instead.

Ensure that you store any backed-up secret keys in a secure location and in an encrypted form
(typically in a password manager).

Downloading keys from the default sett keyserver
------------------------------------------------

*sett* allows user to easily search and retrieve public keys from the keyserver specified in the
:ref:`configuration file<Configuration options>`.

.. admonition:: BioMedIT

  By default, *sett* is configured to search and download keys from the BioMedIT keyserver at
  keyserver.dcc.sib.swiss. No changes in the configuration file are required for BioMedIT users.


Downloading keys using sett-gui
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

To download a public key from the keyserver:

#. In the **Keys** tab, click on **Download keys**. A dialog box will open, allowing to search
   for public keys stored on the keyserver.

#. In the search field, enter either the user name, email address or fingerprint of the
   public key you are looking for and hit **Search**: the matching keys found on the
   keyserver will be displayed.

   .. image:: ../img/sett_download_key_01.*

   For instance, if searching for the key "Bob Testkey <bob@example.com>" with fingerprint
   "AEED7A96F339881F6FE8291464A1E0150613807D", one can search for either "Bob Testkey",
   "bob@example.com" or "AEED7A96F339881F6FE8291464A1E0150613807D". If possible, it is safest
   to search for a key using its fingerprint, since this a unique identifier of a key.

   .. important::

       User names and email addresses indicated on public keys are not unique and offer no
       guarantee of genuineness. Anyone can create a key with a given email or user name.
       For this reason, it is important to **always verify the full fingerprint of a public
       key** against a trusted source before downloading it (e.g. asking the key owner for the
       fingerprint over phone or per email).
       Alternatively, the genuineness of a key can also be verified by looking for a trusted
       signature on the key.

   .. admonition:: BioMedIT

       For BioMedIT users, the search for an email should in principle return a single result.

       **If a search returns multiple results, someone might try to impersonate your recipients'
       key! Please report this to the DCC at biomedit@sib.swiss**

#. When you are confident that the key is genuine, select it and click on **Download**. You
   should now see your recipients' key listed in the **Public keys** box.

#. Select the newly downloaded key in the **Public keys** list and verify that it is marked
   as "This key has been verified", printed in green, as illustrated below.

   **Important:** in order for this verification to work, you must have downloaded the public key
   for the "central authority" defined in your *sett* configuration file. The default "central
   authority" in *sett* is the SPHN Data Coordination Centre (DCC). **BioMedIT users must make
   sure to download the DCC public key - see the**
   :ref:`Downloading the DCC key<Downloading the DCC key>` **section below.**

   .. image:: ../img/sett_certification_status_01.*

Downloading the DCC key
^^^^^^^^^^^^^^^^^^^^^^^

.. admonition:: BioMedIT

    **This section is only relevant for BioMedIT users.** Other users can skip it.

In order to transfer data on the BioMedIT network, **all data sender and recipient keys must be
signed (certified) by the SPHN DCC**. This ensures that all keys involved in the data transfer
procedure are genuine and trustworthy.

BioMedIT users must therefore download the DCC key before they are able to do any encryption,
decryption or data transfer task using *sett*. In principle, the download of the DCC key
should occur automatically when the *sett* application (GUI or CLI) is run.

To download the DCC key manually, enter the following fingerprint as search term::

    B37CE2A101EBFA70941DF885881685B5EE0FCBD3

After downloading the key, verify the full name, email address and fingerprint of the key.
Make sure that it is marked as "This key has been verified", printed in green in the **Keys**
tab when the key is selected in the **Public keys** list. The DCC key should look exactly as in
the figure below.

.. image:: ../img/sett_certification_SPHN.*

If your own key is not yet signed by the DCC, please request a signature following the
instructions given in the
:ref:`Requesting a signature/certification from the SPHN Data Coordination Centre<Requesting a signature/certification from the SPHN Data Coordination Centre>`
section.

Downloading public keys from the BioMedIT keyserver using command line
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The following commands illustrate how to download a public key from the BioMedIT keyserver in
command line. To download keys from a different keyserver, simply replace the URL of the keyserver.

Note: starting with version ``2.2.17`` of ``gpg``, key signatures are no longer automatically
downloaded (due to key poisoning attacks). To force ``gpg`` to download signatures attached to a
key, the ``--keyserver-options no-self-sigs-only,no-import-clean`` option must be added::

    # Search and download key from BioMedIT keyserver.
    # The <search term> can be either a user name, email or fingerprint.
    gpg --keyserver hkp://keyserver.dcc.sib.swiss:80 --keyserver-options no-self-sigs-only,no-import-clean --search-keys <search term>

    # Example: download the public key of the SPHN Data Coordination Centre.
    gpg --keyserver hkp://keyserver.dcc.sib.swiss:80 --keyserver-options no-self-sigs-only,no-import-clean --search-keys B37CE2A101EBFA70941DF885881685B5EE0FCBD3

Alternatively, if the fingerprint of the key to retrieve is known (e.g. because the key owner has
communicated his/her key fingerprint), the ``--recv-key`` option can be used to directly download
the key without first searching for it::

    # Example: download the public key of the SPHN Data Coordination Centre.
    gpg --keyserver hkp://keyserver.dcc.sib.swiss:80 --keyserver-options no-self-sigs-only,no-import-clean --recv-key B37CE2A101EBFA70941DF885881685B5EE0FCBD3

.. note::

    If you are behind a proxy, you will need to extend ``--keyserver-options`` with the
    *http-proxy* option:
    ``--keyserver-options no-self-sigs-only,no-import-clean,http-proxy=http://proxy:port``

After downloading a new key, the signatures on the keys should be checked. If the key is signed by
the DCC the following line should be printed in the list of signatures exactly as shown here:
``sig!3     881685B5EE0FCBD3 2020-03-19 SPHN Data Coordination Centre <dcc@sib.swiss>``

.. code-block:: shell

    # General syntax:
    gpg --check-sigs <key fingerprint or email address>

    # Example:
    gpg --check-sigs alice.smith@example.com

        pub rsa4096 2020-03-27 [SCEA]
            5AF07679EF4BF21F1FDF16C9312F035575A9FF3C
        uid              [ultimate] Alice Test2 <alice@wonder.com>
        sig!3           312F035575A9FF3C 2020-03-27 Alice's test PGP key <alice@sib.swiss>
        sig!3           881685B5EE0FCBD3 2020-03-19 SPHN Data Coordination Centre <dcc@sib.swiss>
        sub     rsa4096 2020-03-27 [SEA]
        sig!             312F035575A9FF3C 2020-03-27 Alice's test PGP key <alice@sib.swiss>

        gpg: 3 good signatures

.. important::

    In case of doubt about the authenticity of a public key, **always check the key's full
    fingerprint** with the key owner. The command to display a key full fingerprint is:
    ``gpg --fingerprint <key ID>``



Uploading public keys to the default sett keyserver
---------------------------------------------------

*sett* allows users to upload public keys to the keyserver specified in the
:ref:`configuration file<Configuration options>`.

.. admonition:: BioMedIT

  By default, *sett* is configured to upload keys to the BioMedIT keyserver at
  keyserver.dcc.sib.swiss. No changes in the configuration file are required for BioMedIT users.


Uploading keys with sett-gui
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Uploading a public key to the keyserver specified in *sett* is straightforward:

#. In the **Keys** tab, select one or more keys to upload from the **Public keys** list.
#. Click on **Upload keys**.
#. A dialog box will appear to ask for confirmation. Verify the selected key is correct
   and click **OK**.

Uploading keys in command line
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Uploading keys via the command line can be done either using *sett* or *GnuPG*. Both options are
shown here.

**sett** command::

    # General syntax:
    sett upload-keys <key fingerprint>

    # Example:
    sett upload-keys AD1008B5F0F876630B9AF245AA2BF155EF144550

**GnuPG** command:
The GnuPG command to upload a public key to the keyserver is the following::

    # General syntax:
    gpg --keyserver hkp://keyserver.dcc.sib.swiss:80 --send-key <key fingerprint>

    # Example:
    gpg --keyserver hkp://keyserver.dcc.sib.swiss:80 --send-key AD1008B5F0F876630B9AF245AA2BF155EF144550



Requesting a signature/certification from the SPHN Data Coordination Centre
---------------------------------------------------------------------------

.. admonition:: BioMedIT

    **This section is only relevant for BioMedIT users.** Other users can skip it.

Requesting a signature using sett-gui
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

To get your key signed by the DCC, complete the following steps:

#. In the **Keys** tab, select your public key in the **Public keys** list. Click **Upload keys**.

#. A dialog box will appear to ask you for confirmation. After having checked that the correct
   key was selected, click **OK**.

#. Two options are available to request that your key be signed:

   #. Press the **Request signature** button.
   #. Send and email to the DCC at biomedit@sib.swiss to request for your key to be signed.
      Please include your full name and key fingerprint in the email.

   Since the DCC needs to manually confirm your identify, the key signing procedure can
   sometimes take up to a few days. You will be notified by email when your key is signed.

#. To update the local copy of your public key with the DCC signature, select it in the
   **Public key** list and then click **Update key**.

#. After your key is updated, you should now see the indication "This key has been verified"
   printed in green, as illustrated in the figure below.

   .. image:: ../img/sett_certification_status_01.*

Your key is now signed and you are ready to start encrypting, transferring and decrypting files.

Requesting a signature in command line
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

After uploading a key to the BioMedIT keyserver, a signature can be requested from the DCC
using the following *sett* command::

    # General syntax:
    sett request-sigs <key fingerprint>

    # Example:
    sett request-sigs AD1008B5F0F876630B9AF245AA2BF155EF144550



Deleting public and private keys
--------------------------------

Deleting keys with sett-gui
^^^^^^^^^^^^^^^^^^^^^^^^^^^

To delete a public key:

#. In the **Keys** tab, select one or more keys to delete from the **Public keys** list.
#. Click **Delete keys** button. A pop-up will open, asking for confirmation. Verify that
   you have selected to correct key, then click **OK** to delete it.

.. note::

    **Why doesn't sett allow to delete private keys ?**
    Deleting private keys can potentially have serious consequences as any data that was encrypted
    with the corresponding public key can no longer be decrypted. For this reason, *sett* does not
    allow users to delete private keys.

    Users wishing to delete a private key with a good reason should refer to the
    :ref:`Deleting keys in command line <Deleting keys in command line>` section of this
    document.

Deleting keys in command line
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The *sett* command line interface does not implement key deletion. Instead, both private and public
keys can be deleted using the ``gpg`` commands illustrated below.

Note that when both a private and public key are present in the keyring, the private key must be
deleted first before the public key can be deleted:

.. code-block:: shell

    # General syntax:
    gpg --delete-key <user ID or fingerprint>           # delete a public key.
    gpg --delete-secret-key <user ID or fingerprint>    # delete a private key.

    # Example:
    gpg --delete-secret-key AD1008B5F0F876630B9AF245AA2BF155EF144550
    gpg --delete-key AD1008B5F0F876630B9AF245AA2BF155EF144550

.. danger::

    **Deleting private keys can have serious consequences** as any data that was encrypted with the
    the corresponding public key can **no longer be decrypted**. Once deleted, there is no way to
    re-generate a given private key. Please make sure you understand the implications before
    deleting any private key.



Revoking PGP keys
-----------------
If a private PGP key has been compromised, is no longer usable (e.g. user lost the password), or
should no longer be used for any other reason, it must be revoked.

A prerequisite for revoking a PGP key is to have generated a
:ref:`revocation certificate<Revocation certificates>`. If the PGP key to revoke was generated with
*sett*, then you should in principle already have a revocation certificate ready to use. If you do
not have a revocation certificate yet, please generate one by referring to the
:ref:`Generating a new key pair in command line<Generating a new key pair in command line>` section.

.. warning::

    **A revoked key cannot be "un-revoked"**. Only revoke a key if you are certain you want to do
    so. Revoked keys can no longer be used to encrypt/decrypt data with *sett*.

Revoking keys with sett-gui
^^^^^^^^^^^^^^^^^^^^^^^^^^^
To revoke a public key in **sett-gui**:

#. In the **Keys** tab, click on **Import key**.
#. Select the *revocation certificate* for the public key you want to revoke. This will import
   the revocation certificate and immediately revoke your key - there is no confirmation requested.
   **Warning:** proceed with caution as **a revoked key cannot be "un-revoked"**.
#. When selecting the revoked key from the **Public keys** list, you should now see a red
   "**key has been revoked**" warning listed under the key's signatures. From this point on, the
   key can no longer be used with *sett*.

   .. image:: ../img/key_management_revoked_key.*

#. If you have previously shared your key via the BioMedIT keyserver (or any other keyserver for
   that matter), you must also re-upload your revoked key to the BioMedIT keyserver (or any other
   keyserver). This will allow other users to update their local copy of your public key, informing
   them that it is no longer valid. To re-upload your revoked public key, select it from the
   **Public keys** list, then click on **Upload keys** to upload it to the keyserver. If your key
   was never present on any keyserver, this step should be skipped.


Revoking keys in command line
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Key revocation is not supported in the *sett* command line and must be done directly with ``gpg``,
as shown in the following example:

.. code-block:: shell

    # Example:
    gpg --import revocation_certificate.asc

After a key has been revoked, it must be uploaded again to any keyserver(s) where it is present,
so that the revocation can be shared with others. This can be done with *sett* as illustrated in
the example below:

.. code-block:: shell

    # Example: a key with fingerprint AD1008B5F0F876630B9AF245AA2BF155EF144550
    # was revoked and must now be uploaded to the keyserver.
    sett upload-keys AD1008B5F0F876630B9AF245AA2BF155EF144550
