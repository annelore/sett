What is sett?
=============

**sett** stands for "Secure Encryption and Transfer Tool" and is a python-based
wrapper around ``GnuPG`` and ``SFTP`` that automates the packaging, encryption,
and transfer of data.

**sett** is available both as a GUI program ``sett-gui``, and in command line
``sett``. Most functionality of *sett* is available in both the GUI and the
command line.

**sett** is developed as part of the `BioMedIT <https://sphn.ch/network/projects/biomedit>`_
project. It is licensed under the GPLv3 (GNU General Public License) and the
source code is available from its public `GitLab repo <https://gitlab.com/biomedit/sett>`_

.. admonition:: BioMedIT

    When using **sett** to transfer data into the **SPHN BioMedIT network**, a
    number of additional constraints apply. These specific instructions are
    highlighted throughout this documentation in
    **BioMedIT labeled boxes, just like this one**.
