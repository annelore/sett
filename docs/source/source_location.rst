Source code
===========

**sett** is licensed under the GPLv3 (GNU General Public License) and the source code
is available at https://gitlab.com/biomedit/sett/.

**sett** is developed as part of the `BioMedIT <https://sphn.ch/network/projects/biomedit/>`_
project.
