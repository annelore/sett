.. _section-sett-config-file:

sett configuration file
=======================

*sett* allows a number of options to be customized via a configuration file. For instance, you
may change the default compression level, or define a default data sender.

Depending on your operating system, the default location for the **sett configuration file** is:

* Linux: ``~/.config/sett/config.json``
* Windows: ``~\AppData\Roaming\sett\config.json``
* Mac OS: ``~/.config/sett/config.json``

Each configuration option has a predefined default value in *sett*, which is used in case the
option is missing from the config file, or if there is no config file. This means that not all
configuration options need to be specified in the config file, but only those for which the default
value should be overridden.

The *sett* configuration settings can be modified either by directly editing the config file,
or by using the ``sett-gui`` application.

Configuration settings that refer to paths (``gpg_home_dir``, ``log_dir``) support the usage of
``~`` as a shortcut for a user's home directory in the path. When *sett* writes or overwrites a
a config file, it will automatically use the ``~`` shortcut so that the file becomes more easily
portable to a different machine.

Using the GUI
-------------

The easiest way to change the *sett* configuration settings is by using the **Settings** tab in
``sett-gui``:

.. image:: ../img/sett_settings.*

Any change made in the **Settings** tab becomes effective immediately. However, to make changes
persistent, the **Save to config file** button at the bottom of the tab must be clicked.
Clicking on **Save to config file** will write the current settings to the config file, making
them persistent across restarts of the application.

Resetting changes made in the **Settings** tab back to the last persisted settings (i.e. the values
in the config file) can be done by clicking on the **Reset** button.

Settings can also be reset to their factory defaults by using the **Restore defaults** button,
e.g. if you config file has become corrupted.

For detailed explanations on the different settings, please refer to the
:ref:`configuration options section<config_options-section>` below.

Using CLI
---------

If no *sett* configuration file is present on your system, it can be created with:

.. code-block:: shell

    sett config --create

To display your current configuration settings, the following command can be used. Note that the
shown values are the combination of the default values, overridden with the values read from the
config file (if a config file exists).

.. code-block:: shell

    sett config --show

For detailed explanations on the different settings, please refer to the
:ref:`configuration options section<config_options-section>` below.

Alternative config file location
--------------------------------

The ``SETT_CONFIG_FILE`` environmental variable allows specifying an alternate location for the
*sett* configuration file. In the example below, the config file location is changed to
``/home/user/custom_sett_config.json``:

.. code-block:: shell

    # Setting a custom config file location for a single command run in a UNIX bash shell.
    SETT_CONFIG_FILE=/home/user/custom_sett_config.json sett ...
    SETT_CONFIG_FILE=/home/user/custom_sett_config.json sett-gui

    # Setting a custom config file location for an entire UNIX bash shell session.
    export SETT_CONFIG_FILE=/home/user/custom_sett_config.json
    sett ...
    sett-gui

    # Setting a custom config file location in a Windows PowerShell. Note that under Windows
    # there should be no quotes around the variable value
    # (e.g. set SETT_CONFIG_FILE="C:\Users\Alice\sett_config.json" will not work).
    set SETT_CONFIG_FILE=C:\Users\Alice\Documents\custom_sett_config.json
    sett ...
    sett-gui

.. _config_options-section:

Configuration options
---------------------

The following options can be set in the config file:

always_trust_recipient_key
    If ``false``, the encryption key must be signed by the local user (``true`` or ``false``)
check_version
    Check whether you have the latest version of *sett* on start-up (``true`` or ``false``)
compression_level
    Default compression level to be used by *sett* when creating new encrypted data packages
    (the data is compressed before it is encrypted). Values must be an integer number in the
    range 0 (no compression) to 9 (maximum compression). Higher compression levels require more
    computing time.
connections
    List of predefined connections for data transfer. Example:

.. code-block:: python

        "connections": {
          "sftp@localhost": {
            "parameters": {
              "destination_dir": "upload",
              "host": "localhost",
              "pkey": "~/.ssh/sftp_ssh_testkey",
              "username": "sftp"
            },
          "protocol": "sftp"
          }
        }

dcc_portal_url
    URL of the **portal** instance. The **portal** is used for
    key signing and **DTR** (Data Transfer Request) validation.
default_sender
    Default sender fingerprint for encryption.
gpg_home_dir
    Path of the directory where **GnuPG** stores its keyrings and other
    configuration settings.
gui_quit_confirmation
    Ask for confirmation to quit `sett-gui` (``true`` or ``false``).
    This option only applies to GUI and does not affect CLI.
key_authority_fingerprint
    ID of the authority key (key ID or ``null``). If a non-``null`` value is
    specified, then only keys signed by this authority could be used.
keyserver_url
    URL of the keyserver: used for publishing/fetching public PGP keys.
gpg_key_autodownload
    Allow the automatic download and refresh of PGP keys from the keyserver
    (``true`` or ``false``).
    If ``true`` (default value), PGP keys used during encryption/decryption are
    attempted to be refreshed from the keyserver before they are used (provided
    a value for ``keyserver_url`` is given).
log_dir
    Path to log files directory
log_max_file_number
    Maximum number of logfiles to keep as backup
max_cpu
    Maximum number of CPU cores for parallel computation. Use ``0`` (default)
    if you want to use all available CPU cores. Note, only some parts of
    the encryption and decryption workflows can parallelize computation.
offline
    In offline mode, sett will not make any network connections: DTR
    verification and automatic PGP key downloading/updating is disabled
    (``true`` or ``false``).
output_dir
    Default output directory, relevant for encryption/decryption.
    This option only applies to GUI.
    In CLI, output location can be specified with optional
    argument ``--output`` (resp. ``-o``) for encryption and
    ``--output-dir`` (resp. ``-o``) for decryption.
package_name_suffix
    Default suffix for encrypted package name. This option applies to both, GUI and CLI.
    In CLI it is possible to override the default suffix with optional argument ``--output-suffix``.
repo_url
    Python package repository, used when looking for updates.
sign_encrypted_data
    Whether encrypted data should be signed with sender's key (``true`` or ``false``).
ssh_password_encoding
    Character encoding used for the SSH key password.
verify_package_name
    If ``true`` (the default), the name of data packages files (i.e. the
    encrypted files) are verified to match the pattern
    ``<project_code>_<date_format>_<package_name_suffix>.zip`` before they
    are transferred.
    This is to verify that no sensitive information has been mistakenly
    included in the file name. This check can be permanently turned-off by
    changing the value of the setting to ``false``.