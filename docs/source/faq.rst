.. _faq-page:

Frequently asked questions and bug reports
==========================================

Bug reports
-----------
**To report a problem with sett** or if you have a question that is **not**
**covered in the present documentation** please open an issue on our public
gitlab repo https://gitlab.com/biomedit/sett/-/issues


Frequently asked questions
--------------------------

Warning "Config: Unexpected keys: ..." at startup
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

* **Problem:** when invoking ``sett`` or ``sett-cli``, a warning appears::

    Config: Unexpected keys: ...

* **Solution:**

  * via GUI: Visit ``Settings`` and press *💾 Save to config file*.
  * Via terminal: Edit your ``config.json`` (see :ref:`section-sett-config-file`)
    and remove the line defining the unexpected variable.

Nothing happens when running *sett-gui* (Windows)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

* **Problem:** when staring *sett-gui* in the terminal, nothing happens and the
  application does not start. In principle, this happens mostly on windows
  machines (on Linux/MacOS, error messages should be displayed in the terminal).

* **Solution:** this generally indicates that an error occurred while the
  *sett-gui* application was launched. On Windows machines, this can be tricky
  to debug since the actual error is not displayed in the terminal.
  However, this problem often stems from a missing dependency, and you should
  therefore make sure that:

  * ``GnuPG`` is installed on your machine - :ref:`see here<installing-sett-page>`.
  * *sett* was installed with all its python dependencies (this should in
    principle be the case if *sett* was installed using ``pip``).

  If this does not resolve the issue, you can try to manually start *sett-gui*
  in an interactive python3 session and see what the error message is:

  1. Start a python3 session on your machine.
  2. Run the following 2 commands, and see which error you get:

     .. code-block:: python

        from sett.gui import run
        run()

Error "No module named 'PySide2'" on *sett* startup (Linux / MacOS)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

* **Problem:** when starting *sett*, the following error is displayed in the
  terminal: ``ModuleNotFoundError: No module named 'PySide2'``. In addition,
  the following error (or something similar) might also be shown:
  ``ImportError: libOpenGL.so.0: cannot open shared object file: No such file or directory``
  or the same error but with ``libGLX.so.0`` instead of ``libOpenGL.so.0``.

* **Solution:** depending on the operating system you are using, this error
  can have multiple causes.

  * **If using a recent operating system that fully supports PySide6**
    If your operating system is reasonably recent and supports ``PySide6``,
    then the underlying problem is not the lack of ``PySide2``, but most likely
    the fact that ``PySide6`` is not correctly installed or is missing a
    dependency.

    To fix the issue, please verify that:

    * ``PySide6`` is installed on your system.
    * The openGL library is installed on your system. OpenGL is an operating
      system library (not a Python module!) required by ``PySide6``.
      However, this library is not always automatically installed when
      ``PySide6`` is installed, and must in some cases be installed manually.

      Here are commands for popular Linux distributions:

      * **Linux Ubuntu or Debian:** ``apt install libopengl0``
      * **Linux Fedora:** ``dnf install libglvnd-opengl libglvnd-glx libglvnd-egl``

  * **If using an older operating system that does not support PySide6**
    If you are using an older operating system (e.g. CentOS 7), then it is
    possible that ``PySide6`` is not supported. In this case, *sett* must be
    run in so-called "legacy" mode, which requires ``PySide2`` to be present on
    your system.

    To fix the issue, please verify that:

    * ``PySide2`` is installed on your system.
    * The "legacy" version of *sett* is installed on your system. The *sett*
      legacy version can be installed with: ``pip install sett[legacy]``.
