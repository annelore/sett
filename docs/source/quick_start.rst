sett quick start guide
======================

Initial setup
-------------

#. Install *sett* following the instructions given in
   :ref:`Installing sett on your local computer<Installing sett on your local computer>`.
#. Run ``sett-gui``.
#. If you do not already possess a private/public PGP key pair, go to the **Keys** tab and create
   one following the instructions given in the
   :ref:`Generating a new public/private PGP key pair<Generating a new public/private PGP key pair>`
   section. You should now see your new key listed in the **Private keys** and **Public keys**
   fields of the **Keys** tab.
#. If not already done, download the public key of the recipient with whom you want to exchange
   data. Go to the **Keys** tab and click on the **Download keys** icon, then search for your
   contact's key by either entering the email or fingerprint associated with your contact's key.
   In all cases, you need to verify the key **fingerprint** before (or just after) downloading it.

   .. admonition:: BioMedIT

      If you are a BioMedIT user, please **request your PGP key to be signed by the DCC**. This
      can be done either at the end of the key generation procedure (a pop-up will ask you whether
      you want to request a signature), or by clicking the **Request signature** icon in the Keys
      tab. For detailed information please see
      :ref:`Requesting a signature/certification from the SPHN Data Coordination Centre<Requesting a signature/certification from the SPHN Data Coordination Centre>`.


Getting a recipient's PGP key
-----------------------------

In order to encrypt data for a given recipient, you must first obtain a copy of their public
PGP key. Public keys are non-sensitive and can be freely exchanged.

#. Go to the **Keys** tab of the *sett* interface and click on the **Download key** icon.
#. A pop-up will appear, were you can search your recipient by name, email, or key fingerprint.
#. Select the key matching your search criteria, and verify that the key **fingerprint** is
   correct (if you don't know what the fingerprint should be, contact the key owner).
#. After selecting the correct key, click **Download**. The recipient's key should now be listed
   under **Public keys**.

.. admonition:: BioMedIT

    For BioMedIT users, **verify that your recipient's key is signed by the DCC**. A green text
    saying "This key has been verified" should be displayed. Within BioMedIT, only keys signed by
    the DCC can be used to encrypt/sign data.

    If the key is signed by the DCC and the email address of the key owner matches their
    institutional email, manual checking of the fingerprint is optional.


sett-gui quick start
--------------------

Encrypting data
^^^^^^^^^^^^^^^

#. Go to the **Encrypt** tab of the *sett* interface.
#. Add one or more files and directories to encrypt by clicking the **Add files** or
   **Add directories** icons.
#. Select your own PGP key in the **Sender** field. This is the key that will be used to sign the
   data.
#. Add one or more **Recipients** by selecting them in the drop-down menu and clicking **+**.
   These are the keys that will be used to encrypt the data (i.e. only these recipients will be
   able to decrypt the data).

   .. admonition:: BioMedIT

       For BioMedIT users, the selected **Recipients** must be officially approved Data Managers
       of the project for which data is being encrypted.

#. **DTR ID:** specifying a valid **Data Transfer Request ID** is mandatory for data to be
   transferred into the BioMedIT network.
   For data not intended to be transferred into the BioMedIT network, the **DTR ID** field can be
   left empty (or set to any arbitrary value), and the **Verify DTR ID** checkbox must be
   disabled (see below).

   .. admonition:: BioMedIT

       For BioMedIT users, the **DTR ID** field is mandatory. Only files encrypted with a valid
       and authorized **DTR ID** value can be transferred into the secure BioMedIT network.
       For this reason, BioMedIT users should always leave the **Verify DTR ID** checkbox enabled.

#. **Verify DTR ID** checkbox: by default, **Verify DTR ID** is enabled and this will enforce the
   following checks (for data not intended to be transferred into the BioMedIT network, this
   checkbox should be disabled):

      * **DTR ID** is valid and the transfer is authorized by the DCC.
      * **Sender** and **Recipients** public PGP keys are signed by the *central authority* defined
        in the :ref:`configuration file<Configuration options>`. By default the central authority
        is the DCC.
      * **Recipients** are officially approved *Data Managers* of the BioMedIT project for which
        data is being encrypted.

#. **Purpose:** purpose of the data transfer, please select either ``PRODUCTION`` or ``TEST``.
   This field is mandatory for data being transferred into the BioMedIT network. For data not
   intended to be transferred into the BioMedIT network, this field can be left empty.
#. **Suffix** and **Location:** optionally, a custom suffix can be specified and will be appended
   to the file name. The location where the output file should be saved can also be specified.
#. **Compression level** slider: select the amount of compression to apply to the input data
   when packaging it. Compression values range between 0 (no compression) and 9 (highest
   compression). Higher compression level result in smaller encrypted output files but require
   more computing time.
   If compression is not required, e.g. because the input data is already in a compressed form,
   then the compression level should be set to 0 in order to speed-up the packaging task.
#. Click **Package & Encrypt** to run the encryption workflow on your data.

Transferring data
^^^^^^^^^^^^^^^^^

#. Go to the **Transfer** tab of the *sett* interface.
#. Select one or more files to transfer using the **Add files** button.
#. Select the transfer **Protocol** to be used (**sftp** or **liquid files**).
#. Enter the required transfer **Parameters** information depending on the selected protocol.
   You should have received these from the data recipient or your local BioMedIT node.
#. Click **Transfer selected files** to start transferring your files.

   .. admonition:: BioMedIT

      For transfers into the BioMedIT network the transfer protocol and associated parameters are
      provided by your local BioMedIT node.

Decrypting data
^^^^^^^^^^^^^^^

#. Go to the **Decrypt** tab of the *sett* interface.
#. Select one or more files to decrypt using the **Add files** button.
#. Specify your desired **Output location**.
#. Click on **Decrypt selected files**.



sett command line quick start
-----------------------------

The main commands to encrypt, transfer and decrypt data with *sett* CLI are given here.
Note that key management is not implemented in the CLI. Please use *sett-gui* or the ``gpg``
command.

.. code-block:: shell

    # Data encryption:
    sett encrypt --sender <sender key fingerprint or email> --recipient <recipient key fingerprint or email> --dtr-id <data transfer ID> --purpose <purpose> --output <output file/directory name> <files or directories to encrypt>

    # Data transfer.
    # to SFTP server:
    sett transfer --protocol=sftp --protocol-args='{"host": "HOST","username":"USERNAME", "destination_dir":"DIR", "pkey":"PRIVATE_RSA_SSH_KEY"}' <files to transfer>
    # to liquid-files server:
    sett transfer --protocol=liquid_files --protocol-args='{"host": "HOST","subject": "SUBJECT", "message": "MESSAGE","api_key":"APIKEY","chunk_size": 100}' <files to transfer>

    # Data decryption:
    sett decrypt --output_dir=<output directory> <files to decrypt>
