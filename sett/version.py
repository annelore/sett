"""This file will be overwritten by CI for releases"""

from datetime import datetime

__version__ = "0.0.0.dev" + datetime.now().strftime("%Y%m%d")
