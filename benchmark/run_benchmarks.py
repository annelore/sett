#!/usr/bin/env python3
import os
import time
import timeit
import shutil
import random
import re
import json
import itertools
import functools
import operator
import tempfile
import logging
import subprocess  # nosec
import csv
import contextlib
import argparse
import multiprocessing
from pathlib import Path
from enum import Enum, EnumMeta
from dataclasses import dataclass, field
from typing import (
    Sequence,
    List,
    Optional,
    Tuple,
    Any,
    Callable,
    Dict,
    Iterable,
    cast,
)
from statistics import mean
from getpass import getpass

import gpg_lite
from libbiomedit.lib.deserialize import deserialize
from sett.utils.config import (
    get_config_dir,
    load_config_dict,
    CONFIG_FILE_ENVIRON_VAR,
    get_config_file,
)
from sett import APP_NAME_SHORT

# Bandit security warnings:
# B404:blacklist import_subprocess
# B603:subprocess_without_shell_equals_true
# This script allows the execution of a user-defined executable - in principle
# sett - through subprocess.run. This becomes a security risk if the script
# was e.g. given SUID permissions. As long as it runs with regular user
# permissions, it does not pose any particular security risk.


CONFIG_FILE_NAME = "benchmark_config.json"
DEFAULT_DATA_SIZE = [100, 200, 500, 1000]
DEFAULT_COMPRESSION_LEVELS = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]


class FileSizeType(Enum):
    SMALL = "small"
    LARGE = "large"
    MIXED = "mixed"


class FileDataType(Enum):
    TEXT = "text"
    BINARY = "binary"


class SettCmd(Enum):
    ENCRYPT = "encrypt"
    DECRYPT = "decrypt"
    TRANSFER = "transfer"


class SftpType(Enum):
    PODMAN = "podman"
    DOCKER = "docker"
    LOCAL = "local"
    OTHER = "other"


class CompCmd(Enum):
    SETT = APP_NAME_SHORT
    PIGZ = "pigz"
    # TODO: SEVENZIP = "7zip"


class ConfigPreset(Enum):
    NONE = "none"
    QUICK = "quick"
    SHORT = "short"
    FULL = "full"


def default_cpu_allocation() -> list:
    """Returns a list of CPUs allocations to test for gzip encryption based
    on the number of CPUs available on the host machine. At most, 3 values are
    returned, and the maximum CPU usage is 16.
     -> machine with 16 CPUs -> [1, 8, 16]
     -> machine with 8 CPUs -> [1, 4, 8]
    """
    host_cpu = multiprocessing.cpu_count()
    cpu_allocation = [x for x in (2, 4, 8, 16) if x <= host_cpu]
    return [1] + (cpu_allocation if len(cpu_allocation) <= 3 else cpu_allocation[-3:])


def get_username() -> str:
    """Retrieve UNIX user name"""
    username = os.path.expandvars("$USER")
    if username != "$USER":
        return username

    raise ValueError("unable to retrieve default USER name from local system")


def get_default_ssh_key() -> Optional[str]:
    """Retrieve default RSA/ED25519 private key on UNIX systems"""
    default_keys = ("id_rsa", "id_ed25519")
    ssh_dir = Path(os.path.expanduser("~/.ssh"))
    for ssh_key in (ssh_dir.joinpath(x) for x in default_keys):
        if ssh_key.is_file():
            return ssh_key.as_posix()

    raise ValueError("unable to retrieve default SSH key from local system")


def generate_unique_file_name(file_path: str) -> str:
    """Create a new file name that does not yet exist on disk.

    Tests whether a file named file_path does yet exist on disk. If such a file
    already exists, new names are suggested by adding incremental numbers at
    the end of the file name, until a file name that does not yet exist is
    found.
    """
    new_file = Path(file_path)
    path, extension = os.path.splitext(file_path)
    x = 0
    while new_file.exists():
        x += 1
        new_file = Path(f"{path}_{x}{extension}")
        if x > 10000:
            raise ValueError("too many iterations looking for filename")

    return new_file.as_posix()


def standardize_enum_seq(seq: Sequence[Any], enum_class: EnumMeta) -> Sequence[Any]:
    """Remove duplicates and sorts value in the same order as they appear in
    the Enum class.
    """
    return [x for x in cast(Iterable[Enum], enum_class) if x in seq]


def validate_int_sequence(
    seq: Sequence[int],
    min_value: Optional[int] = None,
    max_value: Optional[int] = None,
    arg_name: Optional[str] = None,
) -> Sequence[int]:
    """Verify that values in the sequence are in the range [min_value:max_value]"""

    # Customize error message depending on the input values that are present.
    error_msg = f"'{arg_name}'" if arg_name else "sequence"
    error_msg += " values must be integers"
    if min_value is not None:
        error_msg += f" >= {min_value}"
    if max_value is not None:
        error_msg += f" and <= {max_value}"

    # Verify that values are within [min_value:max_value] range.
    if (min_value is not None and min(seq) < min_value) or (
        max_value is not None and max(seq) > max_value
    ):
        raise ValueError(error_msg)

    return seq


def validate_dir(path_to_check: str, arg_name: str) -> str:
    """Expand input path and verify it exists on the local host."""
    path = Path(os.path.expanduser(path_to_check))
    if not path.is_dir():
        raise ValueError(f"'{arg_name}' value is not a valid directory [{path}]")
    return path.as_posix()


def validate_sett_exec(sett_exe: str) -> str:
    """Verify that sett_exec points to an actual sett executable"""

    # Check that that the sett executable can be found.
    sett_exec = shutil.which(sett_exe)
    if not sett_exec:
        raise ValueError(
            f"Unable to find executable for {APP_NAME_SHORT} [{sett_exec}]. "
            f"Make sure to either have the executable in your PATH, or to "
            f"provide the full path to the executable via the [sett_exec] "
            f"argument"
        )
    return sett_exec


@dataclass
class BenchmarkConfig:
    """Dataclass holding the benchmark configuration parameters"""

    data_size: Sequence[int] = field(default_factory=lambda: DEFAULT_DATA_SIZE)
    file_size_type: Sequence[FileSizeType] = field(
        default_factory=lambda: list(FileSizeType)
    )
    file_data_type: Sequence[FileDataType] = field(
        default_factory=lambda: list(FileDataType)
    )

    sett_cmds: Sequence[SettCmd] = field(default_factory=lambda: list(SettCmd))
    compression_levels: Sequence[int] = field(
        default_factory=lambda: DEFAULT_COMPRESSION_LEVELS
    )
    compression_cmd: Sequence[CompCmd] = field(default_factory=lambda: list(CompCmd))
    compression_cpu: Sequence[int] = field(default_factory=default_cpu_allocation)
    checksum_max_cpu: int = 4

    replicates: int = 5
    root_dir: str = os.path.join(os.getcwd(), APP_NAME_SHORT + "_benchmarks")
    sett_exec: str = APP_NAME_SHORT
    result_file_name: str = "benchmark_results.tsv"

    sftp_server: SftpType = SftpType.DOCKER
    sftp_username: str = ""
    sftp_ssh_key: str = ""
    sftp_ask_for_password: bool = True
    sftp_destination_dir: str = "/upload"

    gpg_dir_name: str = "gnupg"
    gpg_key_id: str = "Benchmark Key (disposable test key)"
    gpg_key_email: str = "benchmark.key@example.com"
    gpg_key_passphrase: str = "testkey123"

    template_files_prefix: str = "benchmark"
    template_file_sizes: Dict[int, int] = field(
        default_factory=lambda: {
            1: 200,
            10: 20,
            50: 10,
            100: 10,
            200: 5,
            500: 5,
            1000: 5,
            5000: 2,
        }
    )
    show_stdout: bool = False
    _sftp_ssh_password: str = ""

    def __post_init__(self):
        """Function that is run when a new instance of the class is created"""
        self.validate()

    def validate(self):
        """Verify values of dataclass"""
        # Validate int sequence arguments.
        self.data_size = validate_int_sequence(
            self.data_size, min_value=1, arg_name="data_size"
        )
        self.compression_levels = validate_int_sequence(
            self.compression_levels,
            min_value=0,
            max_value=9,
            arg_name="compression_levels",
        )
        self.compression_cpu = validate_int_sequence(
            self.compression_cpu, min_value=1, arg_name="compression_cpu"
        )

        # Validate Enum sequence arguments.
        # Note that the sett_cmds argument, the ENCRYPT command is mandatory,
        # so we force it's addition here.
        if SettCmd.ENCRYPT not in self.sett_cmds:
            self.sett_cmds.append(SettCmd.ENCRYPT)
        self.file_size_type = standardize_enum_seq(self.file_size_type, FileSizeType)
        self.file_data_type = standardize_enum_seq(self.file_data_type, FileDataType)
        self.sett_cmds = standardize_enum_seq(self.sett_cmds, SettCmd)
        self.compression_cmd = standardize_enum_seq(self.compression_cmd, CompCmd)
        self.sftp_server = SftpType(self.sftp_server)

        # Validate root directory. If unset, the root directory is set to
        # the current working directory.
        self.root_dir = os.path.expanduser(self.root_dir)
        validate_dir(os.path.dirname(self.root_dir), arg_name="root_dir")

        # Validate sett executable path by retrieving the sett version.
        self.sett_exec = validate_sett_exec(self.sett_exec)
        self.sett_version = (  # pylint: disable=attribute-defined-outside-init
            run_subprocess(
                args=[self.sett_exec, "--version"],
                capture_output=True,
                return_stdout=True,
            )
        )

        # Validate SFTP-related arguments. Note that the SSH key of the
        # SFTP server container that we are using is not password protected,
        # hence we set sftp_ssh_key_password to an empty string.
        if self.sftp_server in (SftpType.PODMAN, SftpType.DOCKER):
            self.sftp_username = "sftp"
            self.sftp_ssh_key = os.path.join(self.root_dir, "sftp_ssh_testkey")
            self.sftp_destination_dir = "/upload"
        else:
            if not self.sftp_username:
                self.sftp_username = get_username()
            if not self.sftp_ssh_key:
                self.sftp_ssh_key = get_default_ssh_key()
            if self.sftp_server == SftpType.LOCAL:
                self.sftp_destination_dir = os.path.join(self.root_dir, "local_sftp")

        # Verify the podman/docker dependencies are installed.
        if self.sftp_server in (SftpType.PODMAN, SftpType.DOCKER):
            if not shutil.which(self.sftp_server.value):
                raise ValueError(
                    f"[sftp_server] is set to {self.sftp_server.value}, but "
                    f"'{self.sftp_server.value}' is not available on the local machine"
                )

        # Verify there is enough disk space available.
        original_data_size = self.data_size.copy()
        disk_stats = os.statvfs(self.root_dir)
        free_space_mb = round(disk_stats.f_bavail * disk_stats.f_bsize / 1024 ** 2)
        self.data_size = [x for x in original_data_size if 2 * x < free_space_mb]
        diff = sorted(
            list(set(original_data_size).symmetric_difference(self.data_size))
        )
        if diff:
            logger.warning(
                "# WARNING: not enough free disk space, skipping "
                "the following data sizes: %s\n"
                "# To run a benchmark with a given data size, the available "
                "disk space must be double the data size.",
                ", ".join(str(x) for x in diff),
            )

    def get_ssh_private_key_password_from_user(self):
        if self.sftp_server in (SftpType.LOCAL, SftpType.OTHER):
            if self.sftp_ask_for_password:
                self._sftp_ssh_password = getpass(
                    "Please enter your private SSH key password (used for "
                    "connection to SFTP server).\nIf your SSH key has no "
                    "password, press return without entering anything:"
                )

    @property
    def sett_output(self) -> str:
        return os.path.join(self.root_dir, "benchmark_output.zip")

    @property
    def template_files_dir(self) -> str:
        return os.path.join(self.root_dir, "template_files")

    @property
    def benchmark_files_dir(self) -> str:
        return os.path.join(self.root_dir, "benchmark_files")

    @property
    def gpg_dir(self) -> str:
        return os.path.join(self.root_dir, self.gpg_dir_name)

    @property
    def result_file(self) -> str:
        return os.path.join(self.root_dir, os.path.basename(self.result_file_name))

    @property
    def sftp_protocol_args(self) -> dict:
        """Return a dictionary with SFTP protocol arguments that has the
        following structure
            sftp_protocol_args = '{"host":"localhost:2222",
                                   "username":"sftp",
                                   "destination_dir":"/upload",
                                   "pkey":"~/.ssh/id_rsa"}'
        """
        sftp_host = {
            SftpType.PODMAN: "localhost:2222",
            SftpType.DOCKER: "localhost:2222",
            SftpType.LOCAL: "localhost",
            SftpType.OTHER: self.sftp_server,
        }
        return {
            "host": sftp_host[self.sftp_server],
            "username": self.sftp_username,
            "destination_dir": self.sftp_destination_dir,
            "pkey": self.sftp_ssh_key,
            "pkey_password": self._sftp_ssh_password,
        }

    @property
    def compression_cmds(self) -> Sequence[Tuple[CompCmd, int]]:
        internal_compress = (
            [(CompCmd.SETT, 1)] if CompCmd.SETT in self.compression_cmd else []
        )
        external_compress = list(
            itertools.product(
                [x for x in self.compression_cmd if x != CompCmd.SETT],
                self.compression_cpu,
            )
        )
        return internal_compress + external_compress

    @property
    def combinations(self):
        """Number of parameter combinations for current benchmark settings"""
        return functools.reduce(
            operator.mul,
            map(
                len,
                (
                    self.data_size,
                    self.file_data_type,
                    self.file_size_type,
                    self.compression_levels,
                    self.compression_cmds,
                ),
            ),
        )

    def __str__(self) -> str:
        """Config summary"""
        return (
            f"# Benchmark configuration summary:\n"
            f"# root dir   : {self.root_dir}\n"
            f"# result file: {generate_unique_file_name(self.result_file)}\n"
            f"#\n"
            f"# Combinations to run:\n"
            f"#  -> cmds to benchmark : "
            f"{', '.join(x.value for x in self.sett_cmds)}\n"
            f"#  -> data size [MB]    : "
            f"{', '.join(str(x) for x in self.data_size)}\n"
            f"#  -> data type         : "
            f"{', '.join(x.value for x in self.file_data_type)}\n"
            f"#  -> data size type    : "
            f"{', '.join(x.value for x in self.file_size_type)}\n"
            f"#  -> compression levels: "
            f"{', '.join(str(x) for x in self.compression_levels)}\n"
            f"#  -> compression cmds  : "
            f"{', '.join(map(compression_cmd_as_str, self.compression_cmds))}\n"
            f"#  -> replicates        : {self.replicates}\n"
            f"#  -> combinations      : {self.combinations}\n"
            f"#  -> total runs        : {self.replicates * self.combinations}\n"
            f"#\n"
            f"# sett executable: {self.sett_exec}\n"
            f"# sett version   : {self.sett_version}\n"
            f"# sftp server    : {self.sftp_server.value}\n"
            f"# show stdout    : {self.show_stdout}\n"
            f"#"
        )


def compression_cmd_as_str(x: Tuple[CompCmd, int]) -> str:
    """Converts a tuple of the form (CompCmd.PIGZ, 8) to a string of the
    form "pigz-8cpu."
    """
    return f"{x[0].value}-{x[1]}cpu"


def load_benchmark_config(preset: ConfigPreset = ConfigPreset.NONE) -> BenchmarkConfig:
    """Loads the benchmark config, returning a BenchmarkConfig object"""

    # Load user configuration file. If the file is absent, load_config_dict()
    # returns an empty dict.
    config_file_path = os.path.join(get_config_dir(), CONFIG_FILE_NAME)
    config_dict = load_config_dict(config_file_path)

    # Apply preset, if requested.
    if preset == ConfigPreset.QUICK:
        config_dict.update(
            {
                "data_size": [50, 100],
                "file_size_type": [FileSizeType.SMALL],
                "file_data_type": [FileDataType.TEXT],
                "sett_cmds": list(SettCmd),
                "compression_levels": [0, 4],
                "compression_cmd": [CompCmd.SETT],
                "compression_cpu": [1],
                "replicates": 3,
                "result_file_name": f"benchmark_results_{preset.value}.tsv",
            }
        )
    elif preset == ConfigPreset.SHORT:
        config_dict.update(
            {
                "data_size": [100, 200, 500, 1000, 5000, 10000, 20000, 100000],
                "file_size_type": [FileSizeType.MIXED],
                "file_data_type": [FileDataType.TEXT],
                "sett_cmds": list(SettCmd),
                "compression_levels": [4],
                "compression_cmd": list(CompCmd),
                "compression_cpu": default_cpu_allocation(),
                "replicates": 3,
                "result_file_name": f"benchmark_results_{preset.value}.tsv",
            }
        )
    elif preset == ConfigPreset.FULL:
        config_dict.update(
            {
                "data_size": DEFAULT_DATA_SIZE,
                "file_size_type": list(FileSizeType),
                "file_data_type": list(FileDataType),
                "sett_cmds": list(SettCmd),
                "compression_levels": DEFAULT_COMPRESSION_LEVELS,
                "compression_cmd": list(CompCmd),
                "compression_cpu": default_cpu_allocation(),
                "replicates": 5,
                "result_file_name": f"benchmark_results_{preset.value}.tsv",
            }
        )

    # deserialize() returns an instance of BenchmarkConfig, where any missing
    # field from config_dict is attributed the default value.
    return deserialize(BenchmarkConfig)(config_dict)


def get_pgp_key_fingerprint(
    key_email: str, key_id: str, gpg_dir: str, key_passphrase: str
) -> str:
    """Verify if the specified PGP key exists in the specified directory. If
    not, create a new PGP key

    :param key_email: email associated with PGP key to retrieve/create.
    :param key_id: PGP key ID of the key to retrieve/create.
    :param gpg_dir: path of directory with GPG keyrings.
    :param key_passphrase: password to use for the newly PGP key. Only used
        if a new key is created.
    :return: fingerprint of retrieved/created key.
    :raises ValueError:
    """

    # Attempt to retrieve the PGP key to be used for benchmark runs.
    if not os.path.isdir(gpg_dir):
        os.mkdir(gpg_dir, mode=0o700)

    gpg_store = gpg_lite.GPGStore(gpg_dir)
    gpg_key = gpg_store.list_sec_keys(search_terms=(key_email,))
    if len(gpg_key) == 1:
        return gpg_key[0].fingerprint
    if len(gpg_key) > 1:
        raise ValueError(f"more than one GPG key found for: {key_email}")

    # If the PGP key to run benchmarks is missing, create it.
    logger.info(
        "# Generating new PGP key...\n" "# -> location: %s\n" "# -> key ID  : %s <%s>",
        gpg_dir,
        key_id,
        key_email,
    )
    return gpg_store.gen_key(
        full_name=key_id,
        email=key_email,
        passphrase=key_passphrase,
        key_length=4096,
        key_type="RSA",
    )


def generate_random_files(
    file_sizes: Dict[int, int],
    file_data_type: Sequence[FileDataType],
    file_prefix: str,
    output_dir: str,
    max_size: Optional[int] = None,
) -> None:
    """Generate a set of random files, text or binary.

    :param file_sizes: dictionary of the form {file_size:file_number}, where
        each entry indicates number of files (value) to be generated for a
        given file size in MB (keys).
        E.g. {100:10, 500:2} indicates that the function should create 10
        random 100 MB files and 2 random 500 MB files.
    :param file_data_type: TEXT and/or BINARY. Indicates whether the random
        files should contain text or binary data.
    :param file_prefix: prefix to use when naming the newly created files.
        The files names are constructed following the pattern:
        {file_prefix}_{file_data_type}_{file_size}MB_
    :param output_dir: directory where the generated template files should
        be written.
    :param max_size: if not None, the generation of random files is limited to
        files of size max_size.
    :raises ValueError:
    """

    # If a maximum file size value is passed, remove any unecessary file sizes.
    if max_size:
        file_sizes = {x: file_sizes[x] for x in file_sizes if x <= max_size}

    # Create template files directory, if needed.
    if not os.path.isdir(output_dir):
        os.mkdir(output_dir)

    # Generate new template files as needed. Existing files with the correct
    # name structure are treated as files that have already been generated and
    # are not re-generated. This is to save time.
    not_yet_printed = True
    generate_random_file: Dict[FileDataType, Callable] = {
        FileDataType.TEXT: generate_random_fasta_file,
        FileDataType.BINARY: generate_random_binary_file,
    }
    for file_type in file_data_type:
        prefix = file_prefix + "_" + file_type.value
        extension = ".fasta" if file_type == FileDataType.TEXT else ""

        for file_size, replicates in file_sizes.items():
            for replicate in range(replicates):
                file_name = os.path.join(
                    output_dir, f"{prefix}_{file_size}mb_{replicate + 1}{extension}"
                )
                if not os.path.isfile(file_name):
                    if not_yet_printed:
                        logger.info(
                            "# Generating new random files "
                            "[this takes a while but must only be done once]..."
                        )
                        not_yet_printed = False
                    logger.info(
                        "# -> %s, %s MB", os.path.basename(file_name), file_size
                    )

                    # Generate new random file of the specified size:
                    generate_random_file[file_type](file_name, file_size)


def generate_random_fasta_file(
    file_name: str, file_size: int, fasta_line_length: int = 80
) -> None:
    """Generate a random fasta file.

    :param file_name: path of output fasta file.
    :param file_size: size of output fasta file in megabytes [MB].
    :param fasta_line_length: number of characters (nucleotides) per line of
        the file.
    """
    # Note: generating completely random lines was tested but it took too much
    # time. [nucleotides[random.randint(1, 4)] for y in range(fasta_line_length + 1)]
    nucleotides = {1: "A", 2: "T", 3: "G", 4: "C"}
    lines_min = 100
    lines_max = 1000
    char_count_max = file_size * 1024 ** 2
    sequence_count = 1
    char_count = 0

    # Generate a set of random fasta file lines from which we will later
    # sample. This saves time compared to generating each line completely
    # at random.
    random_line_count = 50000
    random_lines = [
        "".join(
            [
                nucleotides[random.randint(1, 4)]  # nosec
                for y in range(fasta_line_length + 1)
            ]
        )
        for x in range(random_line_count)
    ]

    with open(file_name, "w", encoding="utf-8") as f:
        while char_count < char_count_max:
            line_nb = random.randint(lines_min, lines_max)  # nosec
            sequence_header = ">BENCHMARK_RANDOM_SEQUENCE_" + str(sequence_count) + "\n"
            f.writelines(
                [sequence_header]
                + [
                    random_lines[random.randint(0, random_line_count - 1)]  # nosec
                    + "\n"
                    for x in range(line_nb)
                ]
                + ["\n"]
            )
            char_count += len(sequence_header) + (line_nb * (fasta_line_length + 1)) + 1
            sequence_count += 1


def generate_random_binary_file(file_name: str, file_size: int) -> None:
    """Generate a new file with random binary content.

    :param file_name: path of the new random file.
    :param file_size: file size in MB. Value must be >= 1.
    :raises ValueError:
    """
    if file_size < 1:
        raise ValueError("Value of 'file_size' argument must be >= 1")

    with open(file_name, "wb") as f:
        for _ in range(file_size):
            f.write(os.urandom(1024 ** 2))


def generate_file_set(
    dir_path: str,
    data_size: int,
    file_size_type: FileSizeType,
    template_files_dir: str,
    template_files_prefix: str,
) -> Sequence[str]:
    """Generate a set of test files by copying template files until the
    requested total data size is reached.

    :param dir_path: path of directory to be created and populated with the
        test files. If a directory with the same path already exists, it gets
        overwritten.
    :param data_size: total size of test files to generate in the directory.
        Units are megabytes (MB).
    :param file_size_type: one of 'small', 'large' or 'mixed'.
    :param template_files_dir: path of directory where template files are
        stored.
    :param template_files_prefix: prefix of template files to use.
    :return: number of files of each size in the file set
    :raises ValueError:
    """

    # Create new output directory (overwrite existing directory).
    if os.path.exists(dir_path):
        shutil.rmtree(dir_path)
    os.mkdir(dir_path)

    # Get a dict of template files grouped by size.
    template_files = search_template_files_by_size(
        dir_path=template_files_dir, prefix=template_files_prefix
    )
    if not template_files:
        raise ValueError(
            f"no template files found: dir_path={template_files_dir}, "
            f"prefix={template_files_prefix}."
        )
    template_file_sizes = sorted(template_files.keys())

    # Compute the number of files of each size should be part of the set.
    remaining_size = data_size
    file_count_by_size = {}
    while remaining_size > 0:
        # If 'small' files should be used, or if the remaining data size is
        # too small to accomodate for larger files, use 1 MB files.
        # Otherwise use the largest possible file size.
        if (
            file_size_type == FileSizeType.SMALL
            or len(template_file_sizes) == 1
            or remaining_size < template_file_sizes[1]
            or (
                file_size_type == FileSizeType.MIXED
                and remaining_size > data_size * 0.7
            )
        ):
            file_size = template_file_sizes[0]
        else:
            for file_size in template_file_sizes[::-1]:
                if remaining_size >= file_size:
                    break

        if file_size not in file_count_by_size:
            file_count_by_size[file_size] = 0
        file_count_by_size[file_size] += 1
        remaining_size -= file_size

    # Copy the right number of template files of each size to output directory.
    total_file_count = 0
    file_sizes = sorted(file_count_by_size.keys())
    for file_size in file_sizes:
        for x in range(file_count_by_size[file_size]):
            # Set a limit of 1000 files per sub-directory. When the limit is
            # reached, a new sub-directory is created.
            if total_file_count % 100 == 0:
                subdir = Path(dir_path).joinpath(
                    f"subdir_{round(total_file_count/100) + 1}"
                )
                subdir.mkdir()

            # Copy file to output directory.
            total_file_count += 1
            shutil.copyfile(
                template_files[file_size][x % len(template_files[file_size])],
                subdir.joinpath(
                    f"{template_files_prefix}_{file_size}mb_{total_file_count}"
                ),
            )

    # Return a summary of the number of files of each size in the file set.
    return [
        f"{file_count_by_size[file_size]}x{file_size}MB" for file_size in file_sizes
    ]


def search_template_files_by_size(dir_path: str, prefix: str) -> dict:
    """Search for template files in dir_path and group them by file size.

    This implementation assumes that the name of each template file starts
    with the pattern: "prefix" + "Xmb", where "X" is the size of the file in
    megabytes. For instance, a file named "template_500mb_2.fasta" is assumed
    to have a file size of 500 MB.

    :param dir_path: path of directory where to search for files.
    :param prefix: file name prefix.
    :return: dictonary {file_size:[list of files of given size]}.
    """
    files = []
    for root, _, file_list in os.walk(dir_path):
        files += [os.path.join(root, f) for f in file_list if f.startswith(prefix)]
    files = sorted(files)

    regexp = re.compile("(?<=_)[0-9]+(?=mb)")
    file_sizes = sorted([int(size) for size in set(regexp.findall("".join(files)))])
    return {
        file_size: [f for f in files if f"_{file_size}mb" in f]
        for file_size in file_sizes
    }


def run_subprocess(
    args: Sequence, capture_output: bool, return_stdout: bool = False
) -> Optional[str]:
    """Wrapper for subprocess that checks whether an error occured and, if so,
    raises an error.
    """
    try:
        process = subprocess.run(
            args=args, capture_output=capture_output, check=True, shell=False  # nosec
        )
    except subprocess.CalledProcessError as e:
        raise ValueError(
            f"subprocess command:\n{' '.join(args)}\nfailed with error:\n"
            f"{e.stderr.decode()}"
        ) from e

    if return_stdout:
        return process.stdout.decode("utf-8").strip()

    return None


def run_sett(
    sett_exec: str,
    sett_cmd: SettCmd,
    input_files: Sequence[str],
    output_file: str,
    sender: str,
    sender_passphrase_cmd: str,
    recipients: Sequence[str],
    compression_level: int,
    compression_cmd: Tuple[CompCmd, int],
    sftp_protocol_args: dict,
    show_stdout: bool = True,
) -> None:
    """Run sett with or without external compression.

    :param sett_exec: path of sett executable.
    :param sett_cmd: sett command to run, e.g. "encrypt" or "decrypt".
    :param input_files: list of files to encrypt.
    :param output_file: path of the encrypted sett output file produced by
        the "encrypt" workflow, and to be used by the "decrypt" and "transfer"
        workflows as input.
    :param sender: fingerprint or email of data sender PGP key.
    :param sender_passphrase_cmd: command that returns the sender's private
        PGP key passphrase to the standard output.
    :param recipients: list of emails of fingerprints of data recipients, i.e.
        people for whom the data is encrypted.
    :param compression_level: gzip compression level to use when compressing
        input data.
    :param compression_cmd: compression command and number of CPUs to use. If
        the external compression command value is CompCmd.SETT, data is
        compressed with sett's native compression.
    :param sftp_protocol_args: arguments for the SFTP connection in a JSON
        string format. E.g: {"host":"localhost:2222",
                             "username":"sftp",
                             "destination_dir":"/upload",
                             "pkey":"~/.ssh/id_rsa"}
    :param show_stdout: if True, the sett output (progression bar) is
        printed to the user's terminal.
    :raises ValueError:
    """
    root_dir = os.path.dirname(output_file)
    # Run encrypt workflow.
    if sett_cmd == SettCmd.ENCRYPT:
        # Optionally compress data with an external command. External commands
        # are passed as Tuples (ExtComdCmd, nb_of_cpus).
        if compression_cmd[0] != CompCmd.SETT:
            compressed_input = os.path.join(root_dir, "tmp_compressed.tar.gz")
            run_subprocess(
                args=generate_external_compression_cmd(
                    compress_cmd=compression_cmd[0],
                    compress_cmd_cpu=compression_cmd[1],
                    compression_level=compression_level,
                    input_files=input_files,
                    output_file=compressed_input,
                ),
                capture_output=not show_stdout,
            )
            compression_level = 0
            input_files = [compressed_input]

        # Encrypt data (and possibly compress) with sett.
        run_subprocess(
            args=(
                [
                    sett_exec,
                    sett_cmd.value,
                    "-s",
                    sender,
                    "--passphrase-cmd",
                    sender_passphrase_cmd,
                ]
                + list(
                    itertools.chain(*[["-r", recipient] for recipient in recipients])
                )
                + [
                    "--output",
                    output_file,
                    "--no-verify-dtr",
                    "--compression-level",
                    str(compression_level),
                ]
                + list(input_files)
            ),
            capture_output=not show_stdout,
        )
        # Delete the externally compressed file (if any).
        if compression_cmd[0] != CompCmd.SETT:
            os.unlink(compressed_input)

    # Run decrypt workflow.
    elif sett_cmd == SettCmd.DECRYPT:
        with tempfile.TemporaryDirectory(prefix="decrypted_", dir=root_dir) as tmpdir:
            run_subprocess(
                args=[
                    sett_exec,
                    sett_cmd.value,
                    "--passphrase-cmd",
                    sender_passphrase_cmd,
                    "--output-dir",
                    tmpdir,
                    output_file,
                ],
                capture_output=not show_stdout,
            )

            # If the data was originally compressed with an external command,
            # we must additionally decompress the output produced by sett,
            # because it is still compressed at this point (i.e. sett packaged
            # a compressed file, and so it also outputs a compressed file).
            if compression_cmd[0] != CompCmd.SETT:
                content_dir = os.path.join(
                    tmpdir,
                    os.path.splitext(os.path.basename(output_file))[0],
                    "content",
                )
                run_subprocess(
                    args=[
                        "tar",
                        "--directory",
                        content_dir,
                        "-zxf",
                        os.path.join(content_dir, "tmp_compressed.tar.gz"),
                    ],
                    capture_output=not show_stdout,
                )

    # Run transfer workflow.
    elif sett_cmd == SettCmd.TRANSFER:
        run_subprocess(
            args=[
                sett_exec,
                sett_cmd.value,
                "--protocol",
                "sftp",
                "--protocol-args",
                str(sftp_protocol_args).replace("'", '"'),
                "--no-verify-dtr",
                output_file,
            ],
            capture_output=not show_stdout,
        )

    else:
        raise ValueError(f"unsupported sett command: {sett_cmd}")


def generate_external_compression_cmd(
    compress_cmd: CompCmd,
    compress_cmd_cpu: int,
    compression_level: int,
    input_files: Sequence[str],
    output_file: str,
) -> Sequence[str]:
    """Generates a bash shell command - in the form of a list - that can be
    passed to subprocess.run().

    :param compress_cmd: compression command to use, e.g. "pigz" or "7zip".
    :param compress_cmd_cpu: number of cpus (multithreading) to use.
    :param compression_level: gzip compression level to use [1-9].
    :param input_files: list of files to compress.
    :param output_file: path of output .tar.gz file.
    :returns: Bash shell command in the form of a list.
    :raises ValueError:
    """
    # Retrieve root directory (i.e. common path) of all input files.
    input_root = os.path.commonprefix([os.path.dirname(f) for f in input_files])

    # Generate the bash command for file compression.
    if compress_cmd == CompCmd.PIGZ:
        return [
            "tar",
            f"--use-compress-program=pigz -{compression_level} -kp{compress_cmd_cpu}",
            "-C",
            input_root,
            "-cf",
            output_file,
        ] + [os.path.relpath(f, start=input_root) for f in input_files]

    raise ValueError(f"external compression command [{compress_cmd}] is not supported.")


def write_results_to_file(
    file_path: str,
    timestamp: str,
    data_size: int,
    file_data_type: FileDataType,
    file_size_type: FileSizeType,
    compression_level: int,
    compression_cmd: Tuple[CompCmd, int],
    sett_output_size: int,
    runtimes: Dict[SettCmd, List[int]],
    file_composition: Sequence[str],
    benchmark_replicates: int,
    sett_version: str,
) -> None:
    """Writes results of the current benchmark interation to a file. If the
    specified result file doesn't exits, a new file is created with a header.

    Before writing results to disk, the function also computes the total
    runtime for each "encrypt" + "transfer" + "decrypt" workflow. This
    represents the total time needed to get data from the data provider's
    computer to the data recipient's landing zone.
    Note that values for individual replicates of this "global workflow" are
    only semi-meaningful since there is in fact no specific association between
    the different replicates of each of the steps of the global workflow (e.g.
    there is no specific reason to associate replicate 1 of "encrypt" with
    replicate "1" or "decrypt" rather than say with replicate 2 or "decrypt").
    However, the mean values computed for the global workflow are correct and
    meaningful.
    """

    # Compute total runtime for each  "encrypt" + "transfer" + "decrypt"
    # workflow. This is a list of lists that looks like:
    # [[11, 11, 22], [10, 10, 20], [0, 0, 0], [1, 1, 2]]
    runtimes_data = [[sum(x) for x in zip(*runtimes.values())]] + [
        runtimes[x] for x in list(SettCmd)
    ]

    # Compute, mean, min and max statistics for each workflow (incl. total).
    runtimes_stats = itertools.chain(
        (round(mean(x)) for x in runtimes_data),
        map(min, runtimes_data),
        map(max, runtimes_data),
    )

    # Write values to disk.
    file_does_not_exist = not Path(file_path).is_file()
    with open(file_path, mode="a", encoding="utf-8") as f:
        writer = csv.writer(f, delimiter="\t")

        # If needed, create a new result file with header.
        if file_does_not_exist:
            writer.writerow(
                (
                    "datetime_completed",
                    "data_size[MB]",
                    "data_type",
                    "file_size_type",
                    "compression_level",
                    "compression_cmd",
                    "output_size[MB]",
                    "total_time_mean[sec]",
                    "encrypt_time_mean[sec]",
                    "decrypt_time_mean[sec]",
                    "transfer_time_mean[sec]",
                    "total_time_min[sec]",
                    "encrypt_time_min[sec]",
                    "decrypt_time_min[sec]",
                    "transfer_time_min[sec]",
                    "total_time_max[sec]",
                    "encrypt_time_max[sec]",
                    "decrypt_time_max[sec]",
                    "transfer_time_max[sec]",
                    "total_runtime[sec]",
                    "encrypt_runtimes[sec]",
                    "decrypt_runtimes[sec]",
                    "transfer_runtimes[sec]",
                    "file_composition",
                    "replicates",
                    f"{APP_NAME_SHORT}_version",
                )
            )

        # Add new line to output with results of the current benchmark
        # iteration.
        writer.writerow(
            [
                timestamp,
                data_size,
                file_data_type.value,
                file_size_type.value,
                compression_level,
                compression_cmd_as_str(compression_cmd),
                sett_output_size,
            ]
            + [str(x) for x in runtimes_stats]
            + [str(x) for x in runtimes_data]
            + [
                f"[{', '.join(file_composition)}]",
                benchmark_replicates,
                sett_version,
            ]
        )


class SettConfigOverride:
    """Context manager that points sett towards an alternative configuration
    file where checks for PGP key validation and DTR validation are disabled.
    This is done by:
     * Creating a new, temporary, configuration file.
     * Setting the sett config file environment variable to point to this
       temporary config file.

    :param config_dir: directory where the temporary config file is written.
    :param gpg_dir: temporary GPG keyring to be used for the
        benchmarks.
    """

    def __init__(self, benchark_config: BenchmarkConfig):
        self.config_file = os.path.join(benchark_config.root_dir, "tmp_config.json")
        self.benchmark_config = benchark_config

        # Try loading an existing sett config file.
        orig_config_file = get_config_file()
        if orig_config_file:
            try:
                with open(orig_config_file, mode="r", encoding="utf-8") as f:
                    self.config = json.load(f)
            except IOError as e:
                raise ValueError(
                    f"error opening config file [{orig_config_file}]:"
                ) from e
        else:
            self.config = {}

    def __enter__(self):
        """Create a temporary config file and point the SETT config file
        environment variable to it.
        """

        # Disable all key validation checks. Set new GPG keyring location.
        self.config["key_authority_fingerprint"] = None
        self.config["keyserver_url"] = None
        self.config["offline"] = True
        self.config["gpg_home_dir"] = self.benchmark_config.gpg_dir
        self.config["gpg_key_autodownload"] = False
        self.config["check_version"] = False
        self.config["max_cpu"] = self.benchmark_config.checksum_max_cpu

        # Write temporary config file to disk.
        with open(self.config_file, mode="w", encoding="utf-8") as f:
            json.dump(self.config, f, indent=2)
            f.write("\n")

        # Set environment variable to temporary config file: this will instruct
        # SETT to use the temporary config file instead of the original one.
        os.environ[CONFIG_FILE_ENVIRON_VAR] = self.config_file

    def __exit__(self, exc_type, exc_val, exc_tb):
        """Restore the sett configuration back to its original state, by
        removing the environment variable.
        """
        # Note: apparently os.unsetenv() does not work on all plateforms, so we
        # use del os.environ[]. instead
        if CONFIG_FILE_ENVIRON_VAR in os.environ:
            del os.environ[CONFIG_FILE_ENVIRON_VAR]


class RunSftpServer:
    """Context manager that starts an SFTP server.

    :param sftp_server: Type of local SFTP server to run, one of:
        PODMAN: podman is used to start/stop an SFTP container.
        DOCKER: docker is used to start/stop an SFTP container.
        LOCAL : a local directory is created/deleted, which can be used as
            upload destination for a local SFTP transfer.
        OTHER : no local SFTP server is started.
    :param local_sftp_dir: path to the local directory that will be used as
        SFTP server upload location. Only needed for LOCAL SftpType.
    :param sftp_ssh_key: path where to extract the SSH private key from the
        container image. Only needed for PODMAN and DOCKER SftpType.
    """

    def __init__(
        self,
        sftp_server: SftpType,
        sftp_ssh_key: str,
        local_sftp_dir: str,
    ):
        self.sftp_server = sftp_server
        self.sftp_ssh_key = sftp_ssh_key
        self.local_sftp_dir = local_sftp_dir
        self.container_name = "benchmark_tmp_sftp"

    def __enter__(self):
        """Deploy local SFTP server"""
        # Deploy PODMAN/DOCKER container with SFTP server.
        if self.sftp_server in (SftpType.PODMAN, SftpType.DOCKER):
            run_subprocess(
                args=[
                    self.sftp_server.value,
                    "run",
                    "-d",
                    "--rm",
                    "-p",
                    "2222:22",
                    "--name",
                    self.container_name,
                    "docker.io/biomedit/test-sftp-server:latest",
                ],
                capture_output=True,
            )
            # Extract a copy of the private SSH test key.
            if not os.path.exists(self.sftp_ssh_key):
                run_subprocess(
                    args=[
                        self.sftp_server.value,
                        "cp",
                        f"{self.container_name}:/home/sftp/.ssh/sftp_ssh_testkey",
                        self.sftp_ssh_key,
                    ],
                    capture_output=True,
                )

        # If using a LOCAL SFTP, create a temporary directory for it.
        elif self.sftp_server == SftpType.LOCAL:
            if os.path.exists(self.local_sftp_dir):
                shutil.rmtree(self.local_sftp_dir)
            os.mkdir(self.local_sftp_dir)

    def __exit__(self, exc_type, exc_val, exc_tb):
        """Tear down local SFTP server"""
        if self.sftp_server in (SftpType.PODMAN, SftpType.DOCKER):
            run_subprocess(
                args=[self.sftp_server.value, "stop", self.container_name],
                capture_output=True,
            )
        elif self.sftp_server == SftpType.LOCAL:
            shutil.rmtree(self.local_sftp_dir)


def run_benchmark(preset: ConfigPreset = ConfigPreset.NONE, dryrun=False) -> None:
    """Run the sett benchmarks

    :param preset: use a configuration preset to override user-defined and
        default benchmark parameter values.
    :param dryrun: if True, the benchmark configuration is loaded and
        displayed, but benchmarks are not run.
    """

    # Load and check benchmark parameter values from default and user-provided
    # values.
    config = load_benchmark_config(preset=preset)

    # Display start summary message.
    logger.info("#" * 80)
    logger.info(
        "# Starting %s benchmarks%s", APP_NAME_SHORT, " [dry_run]" if dryrun else ""
    )
    logger.info("# ")
    logger.info(str(config))
    if dryrun:
        logger.info("#" * 80)
        return

    # If the benchmark is using SftpType.LOCAL, ask user for password.
    config.get_ssh_private_key_password_from_user()

    # Create benchmark root directory, if needed.
    if not os.path.isdir(config.root_dir):
        os.mkdir(config.root_dir)

    # Retrieve fingerprint of disposable GPG key used for benchmarks.
    # If the key does not exists, the function creates it.
    gpg_fingerprint = get_pgp_key_fingerprint(
        key_email=config.gpg_key_email,
        key_id=config.gpg_key_id,
        gpg_dir=config.gpg_dir,
        key_passphrase=config.gpg_key_passphrase,
    )

    # Generate a set of random text/binary files of various sizes. These are
    # the "template" file pool from which we will later draw files to generate
    # the input for the benchmarking.
    for file_data_type in config.file_data_type:
        generate_random_files(
            file_sizes=config.template_file_sizes,
            file_data_type=config.file_data_type,
            file_prefix=config.template_files_prefix,
            output_dir=config.template_files_dir,
            max_size=max(config.data_size),
        )

    # Run benchmarks for all requested combinations.
    result_file = generate_unique_file_name(config.result_file)
    for data_size, file_size_type, file_data_type in itertools.product(
        config.data_size, config.file_size_type, config.file_data_type
    ):

        # Create input data files for the current benchmark combination.
        file_composition = generate_file_set(
            dir_path=config.benchmark_files_dir,
            data_size=data_size,
            file_size_type=file_size_type,
            template_files_dir=config.template_files_dir,
            template_files_prefix=(
                f"{config.template_files_prefix}_{file_data_type.value}"
            ),
        )

        # Start loop for compression level and compression command factors.
        for compression_level, compression_cmd in itertools.product(
            config.compression_levels, config.compression_cmds
        ):
            # Display info about current benchmark loop:
            logger.info(
                "# Running benchmark: %s MB - %s %s files - compress-level=%s - "
                "compress-method=%s [%s replicates]",
                str(data_size),
                file_size_type.value,
                file_data_type.value,
                str(compression_level),
                compression_cmd_as_str(compression_cmd),
                config.replicates,
            )

            # Start loop individual sett commands.
            runtimes = {x: [0] * config.replicates for x in list(SettCmd)}
            for sett_cmd in config.sett_cmds:
                logger.info("# -> %s...", sett_cmd.value)

                # Run sett benchmarking with a timer.
                # The SettConfigOverride context manager temporarily overrides
                # the default sett user config file to disable all DTR ID and
                # PGP key related checks. It does so by creating an alternate
                # config file and pointing sett to it.
                run_sftp = (
                    RunSftpServer(
                        sftp_server=config.sftp_server,
                        sftp_ssh_key=config.sftp_ssh_key,
                        local_sftp_dir=config.sftp_destination_dir,
                    )
                    if sett_cmd == SettCmd.TRANSFER
                    and config.sftp_server != SftpType.OTHER
                    else contextlib.nullcontext()
                )
                with run_sftp, SettConfigOverride(config):
                    runtime = timeit.repeat(
                        stmt=lambda: run_sett(
                            sett_exec=config.sett_exec,
                            sett_cmd=sett_cmd,  # pylint: disable=cell-var-from-loop
                            input_files=[config.benchmark_files_dir],
                            output_file=config.sett_output,
                            sender=gpg_fingerprint,
                            sender_passphrase_cmd="echo " + config.gpg_key_passphrase,
                            recipients=[gpg_fingerprint],
                            compression_level=compression_level,  # pylint: disable=cell-var-from-loop
                            sftp_protocol_args=config.sftp_protocol_args,
                            compression_cmd=compression_cmd,  # pylint: disable=cell-var-from-loop
                            show_stdout=config.show_stdout,
                        ),
                        number=1,
                        repeat=config.replicates,
                    )
                    runtimes[sett_cmd] = list(map(round, runtime))

            # When all sett commands for the current run are done,
            # save the benchmark results to file.
            write_results_to_file(
                file_path=result_file,
                timestamp=time.strftime("%Y-%m-%d_%H:%M:%S", time.localtime()),
                data_size=data_size,
                file_data_type=file_data_type,
                file_size_type=file_size_type,
                compression_level=compression_level,
                compression_cmd=compression_cmd,
                sett_output_size=round(
                    Path(config.sett_output).stat().st_size / 1024 ** 2
                ),
                runtimes=runtimes,
                file_composition=file_composition,
                benchmark_replicates=config.replicates,
                sett_version=config.sett_version,
            )

            # Delete sett output.
            if os.path.isfile(config.sett_output):
                os.unlink(config.sett_output)

        # Delete input files used for current loop.
        if os.path.isdir(config.benchmark_files_dir):
            shutil.rmtree(config.benchmark_files_dir)

    logger.info("# ")
    logger.info("# Completed benchmarks successfully")
    logger.info("# Results can be found in: %s", result_file)
    logger.info("#" * 80)


def cli_argument_parser():
    """User input options for the CLI"""

    def parse_preset(arg_value: str) -> ConfigPreset:
        presets = {x.value[0]: x for x in list(ConfigPreset)}
        return presets.get(arg_value[0].lower(), ConfigPreset.NONE)

    parser = argparse.ArgumentParser(
        prog="benchmarks.py",
        usage="%(prog)s [options]",
        description=f"Run benchmarks for {APP_NAME_SHORT}",
        formatter_class=argparse.RawTextHelpFormatter,
    )

    parser.add_argument(
        "-p",
        "--preset",
        action="store",
        choices=list(
            itertools.chain(
                *((x.value, x.value[0]) for x in ConfigPreset if x != ConfigPreset.NONE)
            )
        ),
        default=ConfigPreset.NONE.value,
        type=str,
        help=(
            f"Run the benchmarks with a 'preset':\n"
            f" * '{ConfigPreset.QUICK.value}'/'{ConfigPreset.QUICK.value[0]}':\n"
            f"   run a minimal version of the benchmarks - for test purposes.\n"
            f" * '{ConfigPreset.SHORT.value}'/'{ConfigPreset.SHORT.value[0]}':\n"
            f"   run benchmarks with a limited number of parameter combinations.\n"
            f" * '{ConfigPreset.FULL.value}'/'{ConfigPreset.FULL.value[0]}':\n"
            f"   run benchmarks with all possible parameter combinations."
        ),
    )

    parser.add_argument(
        "-d",
        "--dry-run",
        action="store_true",
        dest="dry_run",
        help="Display parameter combinations that would be run, \n"
        "without running the actual benchmarks.",
    )
    args = parser.parse_args()
    args.preset = parse_preset(args.preset)
    return args


if __name__ == "__main__":

    # Setup logger.
    logger = logging.getLogger()
    logger.setLevel(logging.DEBUG)
    logger.handlers.clear()
    handler = logging.StreamHandler()
    handler.setLevel(logging.INFO)
    formatter = logging.Formatter("%(message)s")
    handler.setFormatter(formatter)
    logger.addHandler(handler)

    # Get user input and run benchmarks.
    user_input = cli_argument_parser()
    run_benchmark(preset=user_input.preset, dryrun=user_input.dry_run)
