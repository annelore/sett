[![pipeline status](https://gitlab.com/biomedit/sett/badges/master/pipeline.svg)](https://gitlab.com/biomedit/sett/-/commits/master)
[![coverage report](https://gitlab.com/biomedit/sett/badges/master/coverage.svg)](https://gitlab.com/biomedit/sett/-/commits/master)
[![documentation status](https://readthedocs.org/projects/sett/badge/)](https://sett.readthedocs.io/)
[![license](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)
[![python version](https://img.shields.io/pypi/pyversions/sett.svg?logo=python&logoColor=white)](https://pypi.org/project/sett)
[![latest version](https://img.shields.io/pypi/v/sett.svg)](https://pypi.org/project/sett)
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)

# SETT: Secure Encryption and Transfer Tool

_sett_ enables packaging, encryption, and transfer of data to preconfigured locations.
Detailed documentation as well as a quick-start guide can be found in the
[sett documentation](https://sett.readthedocs.io).

## Requirements and installation

See <https://sett.readthedocs.io/en/stable/installation.html>

## Creating and managing GnuPG keys with sett

See <https://sett.readthedocs.io/en/stable/key_management.html>

## sett usage

See <https://sett.readthedocs.io/en/stable/usage.html>

## Devel documentation

For the latest, non-stable, version of the docs, see
<https://sett.readthedocs.io/en/latest/>
