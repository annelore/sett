# Contributing to sett

## Development Setup

### Installation

After cloning the repository install the package in editable mode
(it allows trying your changes without reinstalling the package).

```bash
pip install -e .
```

Install development dependencies.

```bash
pip install pre-commit bandit mypy pylint tox
```

### Code style

This project uses [black](https://github.com/psf/black) for code formatting.
For your convenience, you can install a pre-commit hook for black
(for pre-commit installation instruction refer to
[documentation](https://pre-commit.com/#install)).

```bash
pre-commit install
```

In addition we use additional tools for static code analysis.
You can run them locally with.

```bash
bandit -r sett/
mypy sett/ test/
pylint sett/ test/
```

### Testing

To test the code locally, run:

```bash
tox
```

## Commit Message Guidelines
This project follows the [Angular commit message guidelines](https://github.com/angular/angular/blob/master/CONTRIBUTING.md#-commit-message-guidelines)
for its commit messages.


### Template
Fields shown in `[square brackets]` are optional.
```
type[(scope)]: subject line - max 100 characters

[body] - extended description of commit that can stretch over
multiple lines. Max 100 character per line.

[footer] - links to issues with (Closes #, Fixes #, Relates #) and BREAKING CHANGE:
```


### Type and keywords summary
##### Type
The following types are allowed. Only commits with types shown in **bold** are automatically
added to the changelog (and those containing the `BREAKING CHANGE: ` keyword):
* **feat**: new feature
* **fix**: bug fix
* build: changes that affect the build system or external dependencies.
* ci: changes to CI configuration files and scripts.
* docs: documentation only changes
* perf: code change that improves performance.
* refactor: code change that neither fixes a bug nor adds a feature
* style: change in code formatting only (no effect on functionality).
* test: change in unittest files only.

##### Scope
* name of the file/functionality/workflow affected by the commit.

##### Subject line
* one line description of commit with max 100 characters.
* use the imperative form, e.g. "add new feature" instead of "adds new feature" or
  "added a new feature".
* no "." at the end of subject line.

##### body
* Extended description of commit that can stretch over multiple lines.
* Max 100 characters per line.
* Explain things such as what problem the commit addresses, background info on why the change
  was made, alternative implementations that were tested but didn't work out.

##### footer
* Reference to git issue with `Closes/Close`, `Fixes/Fix`, `Related`.
* Location for `BREAKING CHANGE: ` keyword. Add this keyword followed by a description of what
  the commit breaks, why it was necessary, and how users should port their code to adapt to the
  new version. Any commit containing the pattern `BREAKING CHANGE: ` will be added to the
  changelog, regardless of its type.


### Commit messages and auto-versioning
The following rules are applied by the auto-versioning system to modify the version number when a
new commit is pushed to the `master` branch:
* Keyword `BREAKING CHANGE: `: increases the new major digit, e.g. 1.1.1 -> 2.0.0
* Type `feat`: increases the minor version digit, e.g. 1.1.1 -> 1.2.0
* Type `fix`: increases the patch digit, e.g. 1.1.1 -> 1.1.2

**Note:** an exception to the behavior of `BREAKING CHANGE: ` is for pre-release versions (i.e.
0.x.x). In that case, a `BREAKING CHANGE: ` keyword increases the minor digit rather than the
major digit. For more details, see the
[conventional commits](https://www.conventionalcommits.org/en/v1.0.0/) and the
[semantic versioning](https://semver.org/spec/v2.0.0.html) specifications.


### Examples
```
feat(decrypt): add public key auto-download

Allows sett to auto-download public keys from the keyserver when encrypting/decrypting data
for/from recipients/senders whose key is not available in the user's local keyring.
This feature was added to make the life of users easier, so the encryption and decryption workflows
do not crash when a key is missing.

BREAKING CHANGE: config file was changed with the addition of a new "public_key_auto_download"
    option that enables auto-downloading of public keys from the default keyserver when set to
    'True'.

    To migrate a config file, follow the example below:

        Before:

        config: {
          gpg_dir: '~/.gnupg',
          certicifation_key: 'fingerprint',
        }

        After:

        config: {
          gpg_dir: '~/.gnupg',
          certicifation_key: 'fingerprint',
          allow_key_autodownload: 'False',
        }

Closes #7, #123, Related #23
```

```
fix(gui): make warning message more explicit when key is not signed

Improves the readability of the message displayed to the user when a key is missing the
signature from the central authority.
The message was changed from:
    "Only keys signed by <key fingerprint> are allowed to be used."
to:
    "Only keys signed by <key user ID> <key fingerprint> are allowed to be used. Please get your
     public key signed by <key user ID>."

Closes #141
```

```
docs(CONTRIBUTING): add examples of conventional commit messages

Add a few examples of conventional commit messages to CONTRIBUTING.md, so that people don't have
to click on the "Angular commit message guidelines" link to read the full specifications.
Add a concise summary of the guidelines to provider a reminder of the main types and keywords.

Closes #114, #139
```
