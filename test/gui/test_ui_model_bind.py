import unittest
from unittest import mock

from sett.gui.listener import DictWithListener


class TestDictWithListener(unittest.TestCase):
    @staticmethod
    def test_that_update_works():
        d = DictWithListener()
        callback_a = mock.Mock()
        callback_b = mock.Mock()
        d.add_listener("a", callback_a)
        d.add_listener("b", callback_b)
        d.update({"a": 1, "c": 1})
        callback_a.assert_called_once()
        callback_b.assert_not_called()

    @staticmethod
    def test_that_setitem_works():
        d = DictWithListener()
        callback_a = mock.Mock()
        callback_b = mock.Mock()
        d.add_listener("a", callback_a)
        d.add_listener("b", callback_b)
        d["a"] = 1
        d["c"] = 1
        callback_a.assert_called_once()
        callback_b.assert_not_called()
