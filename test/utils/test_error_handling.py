import unittest
from unittest import mock
import logging
import sys

from sett.utils import error_handling
from sett.utils.config import default_config
from sett.core.error import UserError


class MockLoggingHandler(logging.Handler):
    """Mock logging handler to check for expected logs."""

    def __init__(self, *args, **kwargs):
        self.messages = {}
        super().__init__(*args, **kwargs)

    def emit(self, record):
        self.messages.setdefault(record.levelname.lower(), []).append(
            record.getMessage()
        )


class TestErrorHandling(unittest.TestCase):
    def test_exit_on_exception(self):
        @error_handling.exit_on_exception
        def f():
            raise error_handling.UserError("TestError")

        with self.assertRaises(SystemExit):
            f()

    def test_suppress_exceptions(self):
        @error_handling.suppress_exceptions
        def f():
            raise error_handling.UserError("TestError")

        f()

        @error_handling.suppress_exceptions
        def g():
            sys.exit(1)

        with self.assertRaises(SystemExit):
            g()

    def test_log_exceptions(self):
        logger = logging.getLogger("test")
        logger.propagate = False
        mock_handler = MockLoggingHandler()
        logger.addHandler(mock_handler)
        error_obj = UserError("TestError")

        @error_handling.log_exceptions(logger=logger)
        def f():
            raise error_obj

        with self.assertRaises(UserError):
            f()
        self.assertEqual(mock_handler.messages, {"error": ["TestError"]})

    @mock.patch("sett.utils.error_handling.write_error_report")
    def test_error_report_on_exception(self, mock_write_error_report):
        with self.assertRaises(error_handling.ExceptionWithReport):
            with error_handling.error_report_on_exception(default_config()):
                raise RuntimeError("💥")
        mock_write_error_report.assert_called_once()

    @mock.patch("sett.utils.error_handling.atexit.register")
    @mock.patch("sett.utils.error_handling.write_error_report")
    def test_error_report_hint_at_exit(
        self, mock_write_error_report, mock_atexit_register
    ):
        exception = RuntimeError("💥")

        @error_handling.error_report_hint_at_exit(default_config())
        def f():
            raise exception

        with self.assertRaises(RuntimeError, msg="💥"):
            f()
        mock_write_error_report.assert_called_once()
        mock_atexit_register.assert_called_once()
