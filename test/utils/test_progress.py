import unittest
from unittest import mock
import io

from sett.utils import progress
from sett.utils.progress import weights_to_ranges


class TestOpener(unittest.TestCase):
    def test_opener(self):
        mock_f_obj = mock.MagicMock()
        mock_f_obj.__enter__ = mock.Mock(return_value=42)
        mock_open = mock.Mock(return_value=mock_f_obj)
        with mock.patch("builtins.open", mock_open):
            lazy_f_obj = progress.Opener("/tmp/test.txt", "mode")
            self.assertEqual(lazy_f_obj.name, "/tmp/test.txt")
            self.assertEqual(lazy_f_obj.mode, "mode")
            mock_open.assert_not_called()
            with lazy_f_obj as f:
                self.assertEqual(f, 42)
        mock_open.assert_called_once()
        mock_f_obj.__enter__.assert_called_once()
        mock_f_obj.__exit__.assert_called_once()


class TestProgress(unittest.TestCase):
    def test_scale_progress(self):
        self.assertEqual(
            progress.scale_progress(
                None,
                0.0,
                1,
            ),
            None,
        )
        p = progress.ProgressInterface()
        p.update = mock.Mock()
        sub_progress = progress.scale_progress(p, -1.0, 1.0)
        sub_progress.update(0.0)
        sub_progress.update(0.5)
        sub_progress.update(1.0)
        p.update.assert_has_calls([mock.call(-1.0), mock.call(0.0), mock.call(1.0)])

    @staticmethod
    def test_progress_iter():
        p = progress.ProgressInterface()
        p.update = mock.Mock()
        n = 3
        p_iter = progress.progress_iter(range(n), p)
        for _ in p_iter:
            pass
        p.update.assert_has_calls([mock.call(i / n) for i in range(n + 1)])

    def test_progress_iter_with_sub_progress(self):
        p = progress.ProgressInterface()
        p.update = mock.Mock()
        n = 3
        rng = (range(n),) * n

        for sub_rng, sub_progress in progress.progress_iter_with_sub_progress(rng, p):
            for _ in progress.progress_iter(sub_rng, sub_progress):
                pass

        for call, pos in zip(
            p.update.call_args_list,
            ((i + j / n) / n for i in range(n) for j in range(n + 1)),
        ):
            (arg,), _ = call
            self.assertAlmostEqual(arg, pos)

    def test_subprogress(self):
        # Create a simple implementation of a progress interface.
        class SimpleProgressBar(progress.ProgressInterface):
            def __init__(self):
                self._completed_fraction = 0
                self._label = ""

            def update(self, completed_fraction: float) -> None:
                self._completed_fraction = completed_fraction

            def get_completed_fraction(self) -> float:
                return self._completed_fraction

        # Create a new instance of a simple progress bar.
        p = SimpleProgressBar()
        completion_values = (0, 0.1, 0.5, 0.7, 1)
        subprogress_step_values = (0.5, 0.3, 0.2)
        step_nb = 0

        # Loop through the 2 subprogress steps of the progress bar.
        for increase in subprogress_step_values:
            step_nb += 1
            # Compute start and stop points for the current subprogress bar.
            # We use these values to test that the subprogress bar returns
            # the correct values.
            start = sum(subprogress_step_values[: step_nb - 1])
            stop = start + increase

            # Initialize subprogress bar.
            with progress.subprogress(
                progress=p, step_completion_increase=increase
            ) as subp:
                for x in completion_values:
                    subp.update(x)
                    # Verify the completed fraction of the global progress
                    # bar is correct.
                    expected = start + x * (stop - start)
                    self.assertEqual(
                        p.get_completed_fraction(),
                        expected,
                        f"global fraction should be {expected}",
                    )
                    # Verify the completed fraction of the subprogress bar
                    # is correct. The completed fraction
                    self.assertAlmostEqual(
                        subp.get_completed_fraction(),
                        x,
                        msg=f"subp fraction should be {x}",
                    )

        # Test the function when progress is None.
        p = None
        with progress.subprogress(progress=p, step_completion_increase=1) as _:
            pass

    def test_with_progress(self):
        p = progress.ProgressInterface()
        p.update = mock.Mock()
        for char, IoType in ((".", io.StringIO), (b".", io.BytesIO)):
            with self.subTest(type=IoType):
                f = IoType(char * 10)
                f_progress = progress.with_progress(f, p)
                self.assertEqual(f_progress.read(3), char * 3)
                p.update.assert_called_once_with(3 / 10)
                p.update.reset_mock()
                self.assertEqual(f_progress.read(), char * 7)
                p.update.assert_called_once_with(1.0)
                p.update.reset_mock()

        f = progress.with_progress(io.StringIO("1\n2\n3\n"), p)
        self.assertEqual(f.readlines(), ["1\n", "2\n", "3\n"])
        p.update.assert_called_once_with(1.0)
        p.update.reset_mock()

        f = io.StringIO("1\n2\n3\n")
        for i, line in enumerate(progress.with_progress(f, p), 1):
            self.assertEqual(line, str(i) + "\n")
            p.update.assert_called_once_with(i / 3)
            p.update.reset_mock()

    def assertListAlmostEqual(self, list1, list2, tol):
        self.assertEqual(len(list1), len(list2))
        for a, b in zip(list1, list2):
            if isinstance(a, (list, tuple)):
                self.assertListAlmostEqual(a, b, tol)
            else:
                self.assertAlmostEqual(a, b, tol)

    def test_weights_to_ranges(self):
        for weights, expected in (
            ([2, 1, 1], [(0.0, 0.5), (0.5, 0.75), (0.75, 1.0)]),
            ([1], [(0.0, 1.0)]),
            ([0], [(0.0, 1.0)]),
            ([0, 0], [(0.0, 0.5), (0.5, 1.0)]),
        ):
            ranges = list(weights_to_ranges(weights))
            self.assertEqual(len(ranges), len(weights))
            self.assertListEqual(ranges, expected)
        for weights, expected in (
            ([20, 0], [(0.0, 0.99), (0.99, 1.0)]),
            ([0, 20], [(0.0, 0.05), (0.05, 1.0)]),
        ):
            ranges = list(weights_to_ranges(weights))
            self.assertEqual(len(ranges), len(weights))
            self.assertListAlmostEqual(ranges, expected, 1)
