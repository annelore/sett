import json
import os
import io
import unittest
from typing import Dict
from unittest import mock
from unittest.mock import patch
from pathlib import Path

from sett.utils import config
from sett.utils.config import Config, Connection, ConnectionStore, config_to_dict
from sett.core.error import UserError


class TestConfig(unittest.TestCase):
    @staticmethod
    @patch("sett.utils.config.sys_config_dict", lambda: {})
    def test_default_config_serializable():
        json.dumps(config_to_dict(config.default_config()))

    def test_load_config(self):
        home_dir = os.path.expanduser("~")
        default_gpg_path = os.path.join(home_dir, ".gnupg")
        custom_gpg_path = os.path.join(home_dir, "custom", "path", ".gnupg")
        repo_url = "https://pypi.org"

        # Test loading data from a system-wide config file.
        #  * values from the config should override default values.
        #  * when loading URL with a trailing "/", that "/" should be stripped.
        #  * home dir shortcuts ("~") in paths should be expanded.
        for sys_config_content in (
            {},
            {
                "repo_url": repo_url,
                "gpg_home_dir": default_gpg_path,
            },
            {
                "repo_url": repo_url + "/",
                "gpg_home_dir": os.path.join("~", ".gnupg"),
                "gui_quit_confirmation": False,
            },
        ):
            with mock.patch(
                "sett.utils.config.sys_config_dict", lambda: sys_config_content
            ), mock.patch("sett.utils.config.load_config_dict", lambda _: {}):
                cfg = config.load_config()
                self.assertEqual(cfg.default_sender, None)
                if "repo_url" in sys_config_content:
                    self.assertEqual(cfg.repo_url, repo_url)
                if "gpg_home_dir" in sys_config_content:
                    self.assertEqual(cfg.gpg_home_dir, default_gpg_path)
                if "gui_quit_confirmation" in sys_config_content:
                    self.assertEqual(cfg.gui_quit_confirmation, False)
                else:
                    self.assertEqual(cfg.gui_quit_confirmation, True)

        # Test loading data from a user config file.
        #  * values from the user config should override values from the
        #    system-wide config.
        #  * when loading URL with a trailing "/", that "/" should be stripped.
        #  * home dir shortcuts ("~") in paths should be expanded.
        sys_config_content = {
            "repo_url": repo_url,
            "gpg_home_dir": os.path.join("~", ".gnupg"),
            "gui_quit_confirmation": True,
        }
        for user_config_content in (
            {
                "default_sender": "A" * 40,
                "log_dir": os.path.join("~", ".local", "log"),
            },
            {
                "default_sender": "A" * 40,
                "log_dir": os.path.join("~", ".local", "log"),
                "repo_url": "https://alternative.pypi.org/",
                "gpg_home_dir": os.path.join("~", "custom", "path", ".gnupg"),
                "gui_quit_confirmation": False,
            },
        ):
            with mock.patch(
                "sett.utils.config.sys_config_dict", lambda: sys_config_content
            ), mock.patch(
                "sett.utils.config.load_config_dict",
                lambda _: user_config_content,  # pylint: disable=cell-var-from-loop
            ):
                cfg = config.load_config()
                self.assertEqual(cfg.default_sender, "A" * 40)
                self.assertEqual(cfg.log_dir, os.path.join(home_dir, ".local", "log"))

                if "repo_url" in user_config_content:
                    self.assertEqual(cfg.repo_url, "https://alternative.pypi.org")
                else:
                    self.assertEqual(cfg.repo_url, repo_url)

                if "gpg_home_dir" in user_config_content:
                    self.assertEqual(cfg.gpg_home_dir, custom_gpg_path)
                else:
                    self.assertEqual(cfg.gpg_home_dir, default_gpg_path)

                if "gui_quit_confirmation" in user_config_content:
                    self.assertEqual(cfg.gui_quit_confirmation, False)
                else:
                    self.assertEqual(cfg.gui_quit_confirmation, True)

    # get_config_file() tests:
    # Case 1: when the config file path environmental variable is not defined,
    # the function should return the default config file location.
    def test_get_config_file_from_default_path(self):
        config_path = config.get_config_file()
        default_path = os.path.join(config.get_config_dir(), config.CONFIG_FILE_NAME)
        self.assertEqual(config_path, default_path)

    # Case 2: when a custom location is passed via the appropriate
    # environmental variable, get_config_file() should now return that
    # custom location.
    @patch.dict(
        os.environ, {config.CONFIG_FILE_ENVIRON_VAR: "~/fake/path/sett_config.json"}
    )
    def test_get_config_file_from_environ(self):
        config_path = config.get_config_file()
        self.assertEqual(config_path, os.environ.get(config.CONFIG_FILE_ENVIRON_VAR))

    # Case 3: a custom location is passed via the environmental variable,
    # but it points to a directory rather than a file. This should raise
    # an error.
    @patch.dict(os.environ, {config.CONFIG_FILE_ENVIRON_VAR: "~/fake/directory"})
    @patch("os.path.isdir", return_value=True)
    def test_get_config_file_from_environ_bad_input(self, mock_isdir):
        self.assertRaises(UserError, config.get_config_file)
        mock_isdir.assert_called_once_with(
            os.environ.get(config.CONFIG_FILE_ENVIRON_VAR)
        )

    def test_reverse_expanduser(self):
        home_dir = Path.home()
        path_sep = os.sep
        home_dir_split = home_dir.as_posix().strip("/").split("/")
        not_home_dir_path = os.path.join("not", "a", "home", "dir", "path")
        bad_home_dir_path = os.path.join("a", "bad", *home_dir_split, "path")

        # Note on test paths and their expected values:
        # * Paths 1 and 2 start with a proper home directory, which should
        #   be replaced by "~".
        # * Path 3 is missing the leading separator. It should not be modified.
        # * The remaining path do not start in the home directory. Their values
        #   should not be modified except for the removal of trailing
        #   separators.
        test_paths = (
            (
                path_sep + os.path.join(*home_dir_split, "some", "path"),
                os.path.join("~", "some", "path"),
            ),
            (
                path_sep
                + os.path.join(*home_dir_split, "some", "relative", "..", "path"),
                os.path.join("~", "some", "relative", "..", "path"),
            ),
            (
                os.path.join(*home_dir_split, "some", "path"),
                os.path.join(*home_dir_split, "some", "path"),
            ),
            (not_home_dir_path, not_home_dir_path),
            (path_sep + not_home_dir_path, path_sep + not_home_dir_path),
            (path_sep + not_home_dir_path + path_sep, path_sep + not_home_dir_path),
            (bad_home_dir_path, bad_home_dir_path),
        )
        for input_path, expected_path in test_paths:
            self.assertEqual(config.reverse_expanduser(input_path), expected_path)

    def test_config_to_dict(self):
        # Verify that paths values that point to the user's home directory
        # are replaced with the "~" shortcut.
        settings_that_are_path = ("gpg_home_dir", "log_dir", "output_dir")
        test_config = Config()
        home_dir = os.path.expanduser("~")

        # Set arbitrary paths located in the home directory, to see if they
        # are properly converted to "~" during config file writing.
        for setting in settings_that_are_path:
            test_config.__setattr__(setting, os.path.join(home_dir, "subdir", setting))

        # Convert to dict, then check that the serialization has converted
        # home dir path to "~".
        config_as_dict = config_to_dict(test_config)
        for setting in settings_that_are_path:
            self.assertEqual(
                config_as_dict[setting], os.path.join("~", "subdir", setting)
            )

    def test_save_config(self):  # pylint: disable=no-self-use
        # Verify that the config dict passed to the function is written to
        # to correct file path.
        config_dict = config_to_dict(Config())
        config_dict_expected = config_dict.copy()

        test_config_path = "/test/path/config.json"
        with mock.patch(
            "builtins.open", new_callable=mock.mock_open()
        ) as m, mock.patch("json.dump") as m_json, mock.patch(
            "pathlib.Path.is_dir", lambda _: True
        ):
            config.save_config(config=config_dict, path=test_config_path)

        # Note: ".__enter__.return_value" is here needed because the call to
        # builtin open() is made inside a context manager.
        m_json.assert_called_once_with(
            config_dict_expected,
            m.return_value.__enter__.return_value,
            indent=2,
            sort_keys=True,
        )
        m.assert_called_once_with(Path(test_config_path), "w", encoding="utf-8")


class InMemoryConnectionStore(ConnectionStore):
    def __init__(self, cfg: Config):
        super().__init__("")
        self._f = io.StringIO(json.dumps(config_to_dict(cfg)))

    def _read(self) -> Dict:
        return json.loads(self._f.getvalue())

    def _write(self, data: Dict):
        """Write data to config file"""
        self._f.truncate(0)
        self._f.write(json.dumps(data))

    def get_dict(self) -> Dict:
        return self._read()


class TestConnectionStore(unittest.TestCase):
    def setUp(self):
        self.config = Config()
        self.conn1 = Connection("sftp", {})
        self.config.connections["conn1"] = self.conn1
        self.connection_store = InMemoryConnectionStore(self.config)

    def test_rename(self):
        self.connection_store.rename("conn1", "new_conn")
        self.config.connections["new_conn"] = self.config.connections.pop("conn1")
        cfg = self.connection_store.get_dict()
        self.assertEqual(cfg, config_to_dict(self.config))
        self.assertNotIn("conn1", cfg["connections"])
        with self.assertRaises(UserError):
            self.connection_store.rename("foo", "bar")

    def test_delete(self):
        self.connection_store.delete("conn1")
        cfg = self.connection_store.get_dict()
        self.assertEqual(cfg["connections"], {})
        with self.assertRaises(UserError):
            self.connection_store.delete("foo")

    def test_save(self):
        new_label = "new connection"
        params = {"username": "chuck", "host": "localhost", "password": "123"}
        conn = Connection("sftp", params)
        self.connection_store.save(new_label, conn)
        cfg = self.connection_store.get_dict()
        self.assertEqual(
            sorted(cfg["connections"].keys()), sorted(["conn1", new_label])
        )
        self.assertNotIn("password", cfg["connections"][new_label])
        self.assertEqual(
            cfg["connections"][new_label]["parameters"],
            {k: v for k, v in params.items() if k != "password"},
        )
