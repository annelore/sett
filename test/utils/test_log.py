import unittest
from unittest import mock
import logging

from sett.utils import log


class TestLog(unittest.TestCase):
    @staticmethod
    def test_log_task_success():
        mock_log_ok = mock.Mock()
        mock_log_err = mock.Mock()
        with log.log_task("Testing", ok_logger=mock_log_ok, err_logger=mock_log_err):
            pass
        mock_log_err.assert_not_called()
        mock_log_ok.assert_has_calls([mock.call("Testing"), mock.call("OK")])

    def test_log_task_error(self):
        mock_log_ok = mock.Mock()
        mock_log_err = mock.Mock()
        with self.assertRaises(ValueError):
            with log.log_task(
                "Testing", ok_logger=mock_log_ok, err_logger=mock_log_err
            ):
                raise ValueError("TestError")
        mock_log_ok.assert_called_once_with("Testing")
        mock_log_err.assert_called_once_with("FAILED")

    def test_create_logger(self):
        class MockLogger:
            error = mock.Mock()
            info = mock.Mock()

        def mock_logdir():
            return "/tmp"

        with mock.patch("logging.Logger.info", MockLogger.info), mock.patch(
            "logging.Logger.error", MockLogger.error
        ), mock.patch("sett.utils.config.get_default_log_dir", mock_logdir()):
            logger = log.create_logger(__name__)
            with self.assertRaises(ValueError), logger.log_task("Testing"):
                raise ValueError("TestError")
            MockLogger.info.assert_called_once_with("Testing")
            MockLogger.error.assert_called_once_with("FAILED")

    def test_sec_to_hours(self):
        param_list = [
            (34598, "9h 36m 38s"),
            (0, "0s"),
            (1, "1s"),
            (60, "1m 0s"),
            (73, "1m 13s"),
            (3600, "1h 0m 0s"),
            (3601, "1h 0m 1s"),
        ]
        for sec, output in param_list:
            with self.subTest(sec=sec, output=output):
                self.assertEqual(log.sec_to_hours(sec), output)

    def test_log_to_memory(self):
        root_logger = logging.getLogger()
        saved_handlers = root_logger.handlers
        root_logger.handlers = []
        sub_logger = logging.getLogger("sublogger")
        with log.log_to_memory() as mem_log:
            sub_logger.debug("*debug*")
            sub_logger.info("*info*")
            sub_logger.warning("*warning*")
        root_logger.handlers = saved_handlers
        log_str = mem_log.getvalue()
        self.assertTrue("*debug*" in log_str)
        self.assertTrue("*info*" in log_str)
        self.assertTrue("*warning*" in log_str)
