import unittest
from unittest.mock import patch, Mock

from sett.core.error import UserError
from sett.workflows.upload_keys import upload_keys
from . import DisableLogging


class TestUploadKeys(unittest.TestCase):
    @patch("sett.workflows.upload_keys.logger")
    @patch("sett.workflows.upload_keys.crypt_upload_keys")
    @patch("sett.workflows.upload_keys.verify_key_length")
    @patch("sett.workflows.upload_keys.search_pub_key")
    def test_upload_keys(
        self,
        mock_search_pub_key,
        mock_verify_key_length,
        mock_crypt_upload_keys,
        mock_logger,
    ):

        mock_config = Mock(offline=False)
        key_ids = ["AAAAAAAAA", "CCCCCCCCC"]
        mock_search_pub_key.side_effect = lambda k, c, sigs: Mock(
            key_id=k, fingerprint=k
        )

        with DisableLogging():
            with self.subTest("Online mode"):
                upload_keys(key_ids, config=mock_config)
                mock_verify_key_length.assert_called()
                mock_crypt_upload_keys.assert_called_once()
                mock_logger.info.assert_called_once()

            with self.subTest("Offline mode"):
                with self.assertRaises(UserError):
                    mock_config.offline = True
                    upload_keys(key_ids, config=mock_config)
