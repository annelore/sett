import os
import unittest
from unittest import mock

from sett.core.error import UserError
from sett.workflows.encrypt import generate_output_archive_name, check_paths_on_posix


class TestEncrypt(unittest.TestCase):
    def test_generate_output_archive_name(self):
        tar_name = generate_output_archive_name("1", default="")
        self.assertIsInstance(tar_name, str)
        self.assertTrue(tar_name.endswith("1.zip"))
        tar_name = generate_output_archive_name("1.zip", default="")
        self.assertTrue(tar_name.endswith("1.zip"))

        self.assertTrue(
            generate_output_archive_name(None, default="default").endswith(
                "default.zip"
            )
        )

        cwd = os.getcwd()
        d = os.path.realpath(
            os.path.join(cwd, "..")
        )  # make sure d is some directory different from cwd
        self.assertEqual(
            generate_output_archive_name(d, default="default"),
            os.path.join(d, "default.zip"),
        )

    def test_check_paths_on_posix(self):
        check_paths_on_posix(("foo/bar.txt", "foo/baz/test.csv"))
        with mock.patch("sett.core.checksum.os.path.sep", "/"), self.assertRaises(
            UserError
        ):
            check_paths_on_posix(("foo/bar.txt", "foo\\baz\\test.csv"))
