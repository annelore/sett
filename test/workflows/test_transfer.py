import unittest
import contextlib

from sett.core.error import UserError
from sett.workflows.encrypt import DATE_FMT_FILENAME
from sett.workflows.transfer import check_archive_name_follows_convention


class TestTransfer(unittest.TestCase):
    def test_check_archive_name_follows_convention(self):

        tests = [
            ("20210908T140302", None, None, True),
            ("demo_project_20210908T140302", "demo_project", None, True),
            ("20210908T140302_test", None, "test", True),
            ("demo_project_20210908T140302_test", "demo_project", "test", True),
            ("/something/random/demo_20210908T140302", "demo", None, True),
            ("20210908T14032", None, None, False),
            ("202198T1432", None, None, False),
            ("2198T1432", None, None, False),
            ("nemo_project_20210908T140302", "demo_project", None, False),
            ("demo_project_20210908T140302_a_secret", "demo_project", "test", False),
            ("demo_project_20210908T140302-a_secret", "demo_project", "test", False),
        ]

        for (
            path,
            code,
            suffix,
            should_pass,
        ) in tests:
            for extension in (".zip", ".tar", ".txt"):

                # When file extension is different from ".zip"/".tar", an error
                # should always be raised.
                if extension == ".txt":
                    should_pass = False

                # Verify that the check succeeds/fails as expected.
                with contextlib.nullcontext() if should_pass else self.assertRaises(
                    UserError
                ) as e:
                    check_archive_name_follows_convention(
                        archive_path=path + extension,
                        project_code=code,
                        package_name_suffix=suffix,
                    )

                # When the check fails, verify the error message is correct.
                if not should_pass:
                    expected_format = "_".join(
                        filter(None, (code, DATE_FMT_FILENAME, suffix))
                    )
                    self.assertIn(expected_format, str(e.exception))
