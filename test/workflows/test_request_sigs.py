import unittest
from unittest.mock import patch, Mock

from sett.core.error import UserError
from sett.workflows.request_sigs import request_sigs
from . import DisableLogging


class TestRequestSigs(unittest.TestCase):
    @patch("sett.workflows.request_sigs.logger")
    @patch("sett.workflows.request_sigs.verify_key_length")
    @patch("sett.workflows.request_sigs.search_pub_key")
    @patch("sett.workflows.request_sigs.upload_keys")
    def test_request_sigs(
        self, mock_upload_keys, mock_search_pub_key, mock_verify_key_length, mock_logger
    ):

        mock_config = Mock(offline=True)
        key_ids = ["AAAAAAAAA", "BBBBBBBBB"]
        mock_search_pub_key.side_effect = lambda k, c, sigs: Mock(
            key_id=k, fingerprint=k
        )

        with DisableLogging():
            with self.subTest("Offline mode"):
                with self.assertRaises(UserError):
                    request_sigs(key_ids, config=mock_config)
                mock_upload_keys.assert_not_called()

            with self.subTest("Online mode"):
                mock_config.offline = False
                request_sigs(key_ids, config=mock_config)
                mock_verify_key_length.assert_called()
                mock_config.portal_api.request_key_signature.assert_called()
                mock_logger.info.assert_called()
                self.assertEqual(mock_upload_keys.call_count, 2)
