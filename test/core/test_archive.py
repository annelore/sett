import io
import tarfile
import tempfile
import unittest
import zipfile
from datetime import datetime
from unittest import mock
from unittest.mock import MagicMock

from sett.core import archive
from sett.core.archive import (
    check_package,
    PACKAGE_CONTENT,
    METADATA_FILE_SIG,
    METADATA_FILE,
    DATA_FILE_ENCRYPTED,
)
from sett.core.error import UserError

ARCHIVE_CONTENT = (
    (METADATA_FILE, b'{"test": true}'),
    (DATA_FILE_ENCRYPTED, b"some initial binary data: \x00\x01"),
    (METADATA_FILE_SIG, b"Some signature"),
)


def make_tar(content, path: str):
    with tarfile.open(path, mode="w") as tar:
        for f_name, f_content in content:
            f_io_content = io.BytesIO(f_content)
            t_info = tarfile.TarInfo(f_name)
            t_info.size = len(f_io_content.getvalue())
            tar.addfile(t_info, f_io_content)


def make_zip(content, path: str):
    with zipfile.ZipFile(path, mode="w") as zip_obj:
        for f_name, f_content in content:
            zip_obj.writestr(
                zipfile.ZipInfo(f_name, date_time=datetime.utcnow().timetuple()[:6]),
                f_content,
            )


class TestArchive(unittest.TestCase):
    def test_extract(self):
        with tempfile.NamedTemporaryFile() as tmp:
            for archive_fn in (make_zip, make_tar):
                with self.subTest(archive_fn.__name__):
                    archive_fn(ARCHIVE_CONTENT, tmp.name)
                    with archive.extract(tmp.name, ARCHIVE_CONTENT[0][0]) as f:
                        self.assertEqual(f.read(), ARCHIVE_CONTENT[0][1])
                    with archive.extract(
                        tmp.name, ARCHIVE_CONTENT[0][0], ARCHIVE_CONTENT[1][0]
                    ) as (f1, f2):
                        self.assertEqual(f1.read(), ARCHIVE_CONTENT[0][1])
                        self.assertEqual(f2.read(), ARCHIVE_CONTENT[1][1])
                    with self.assertRaises(UserError), archive.extract(
                        tmp.name, "missing.txt"
                    ) as f:
                        pass

    def test_extract_with_progress(self):
        with tempfile.NamedTemporaryFile() as tmp:
            for archive_fn in (make_zip, make_tar):
                with self.subTest(archive_fn.__name__):
                    archive_fn(ARCHIVE_CONTENT, tmp.name)
                    with archive.extract_with_progress(
                        tmp.name, None, ARCHIVE_CONTENT[0][0]
                    ) as f:
                        self.assertEqual(f.read(), ARCHIVE_CONTENT[0][1])
                    with archive.extract_with_progress(
                        tmp.name, None, ARCHIVE_CONTENT[0][0], ARCHIVE_CONTENT[1][0]
                    ) as (f1, f2):
                        self.assertEqual(f1.read(), ARCHIVE_CONTENT[0][1])
                        self.assertEqual(f2.read(), ARCHIVE_CONTENT[1][1])
                    with self.assertRaises(UserError), archive.extract_with_progress(
                        tmp.name, None, "missing.txt"
                    ) as f:
                        pass

    def test_check_package(self):
        # Mock
        with mock.patch("sett.core.archive.archive_reader") as patched_reader:
            package_name = "archive_package.zip"

            def namelist(magic_mock: MagicMock, return_list: list):
                magic_mock.return_value.__enter__.return_value.namelist.return_value = (
                    return_list
                )

            with self.subTest("Empty archive"):
                namelist(patched_reader, [])
                with self.assertRaisesRegex(UserError, r"is empty\.$"):
                    check_package(package_name)

            with self.subTest("Missing files"):
                namelist(patched_reader, [METADATA_FILE])
                with self.assertRaisesRegex(
                    UserError, r"Following files are missing\:"
                ):
                    check_package(package_name)

            with self.subTest("Foreign files"):
                namelist(
                    patched_reader,
                    list(PACKAGE_CONTENT.keys()) + ["should_not_be_there.txt"],
                )

                with self.assertRaisesRegex(
                    UserError,
                    r"Following files are NOT part of the specification and are rejected\:",
                ):
                    check_package(package_name)

            with self.subTest("Names MUST be relative"):
                namelist(patched_reader, ["../" + METADATA_FILE, DATA_FILE_ENCRYPTED])
                with self.assertRaisesRegex(
                    UserError,
                    "The archive contains files with absolute path or paths "
                    "ending in a parent directory",
                ):
                    archive.check_package(package_name)

        # Temporary file
        with tempfile.NamedTemporaryFile() as tmp:
            for archive_fn in (make_zip, make_tar):
                with self.subTest(f"Correct archive: {archive_fn.__name__}"):
                    archive_fn(ARCHIVE_CONTENT, tmp.name)
                    archive.check_package(tmp.name)

            with self.subTest("NOT an archive"):
                tmp.truncate()
                tmp.write(b"foo")
                with self.assertRaisesRegex(
                    UserError,
                    "is not a .zip or .tar archive.\n"
                    "Only .zip and .tar files can be used as input.",
                ):
                    archive.check_package(tmp.name)
