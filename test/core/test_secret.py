import unittest
from typing import Optional
from dataclasses import dataclass

from sett.core import secret
from sett.core.secret import Secret


class TestSensitive(unittest.TestCase):
    def test_repr(self):
        scr = Secret("123")
        self.assertEqual(repr(scr), "***")

    def test_format(self):
        scr = Secret("123")
        self.assertEqual(f"{scr}", "***")

    def test_assert_str_is_not_secret(self):
        self.assertNotEqual(str, Secret)
        self.assertNotEqual([str], [Secret])
        self.assertNotEqual({"x": str}, {"x": Secret})

    def test_enforce_secret_by_signature_args(self):
        def f(scr: Secret, x: int):
            return scr, x

        args, kwargs = secret.enforce_secret_by_signature(f, ("123", 1), {})
        self.assertEqual([type(a) for a in args], [Secret, int])
        self.assertEqual(kwargs, {})

    def test_enforce_secret_by_signature_kwargs(self):
        def f(scr: Secret, x: int):
            return scr, x

        args, kwargs = secret.enforce_secret_by_signature(f, (), dict(scr="123", x=1))
        self.assertEqual(args, ())
        self.assertEqual(
            {key: type(val) for key, val in kwargs.items()}, {"scr": Secret, "x": int}
        )

    def test_enforce_secret_by_signature(self):
        def f(scr: Secret, x: int, *, scr2: Secret, y: bool):
            return scr, x, scr2, y

        args, kwargs = secret.enforce_secret_by_signature(
            f, ("123", 1), dict(scr2="123", y=False)
        )
        self.assertEqual([type(a) for a in args], [Secret, int])
        self.assertEqual(
            {key: type(val) for key, val in kwargs.items()}, {"scr2": Secret, "y": bool}
        )

    def test_dataclass(self):
        @dataclass
        class InteriorWithSecret:
            scr: Optional[Secret]
            x: float

        @dataclass
        class WithSecret:
            scr: Secret
            s: str
            interior: InteriorWithSecret

        instance = WithSecret("xxx", "___", InteriorWithSecret("xxx", 1.0))
        self.assertTrue("xxx" in repr(instance))
        self.assertTrue("xxx" not in repr(secret.enforce_secret(instance, WithSecret)))

    def test_optional(self):
        self.assertTrue(
            "xxx" not in repr(secret.enforce_secret("xxx", Optional[Secret]))
        )
