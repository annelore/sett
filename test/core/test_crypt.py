import unittest
from unittest import mock

from sett.core import crypt
from sett.core.secret import Secret
from sett.core.error import UserError
from sett.core.request import urlopen
from sett.utils.config import Config


class TestPassphraseChecks(unittest.TestCase):
    def test_enforce_password(self):
        s = Secret("topsecret")
        self.assertEqual(crypt.enforce_passphrase(s), s)
        self.assertIsInstance(crypt.enforce_passphrase(s), Secret)
        with self.assertRaises(ValueError):
            crypt.enforce_passphrase(None)

    # pylint: disable=no-self-use
    @mock.patch("sett.core.crypt.check_password")
    def test_check_password_matches_any_key_good(self, mock_check_password):
        key1 = mock.Mock
        key1.fingerprint = "AAAA"
        keys = (key1, mock.Mock)
        p = "test"
        store = mock.Mock
        crypt.check_password_matches_any_key(p, keys, store)
        mock_check_password.assert_called_once_with(
            key_fingerprint=key1.fingerprint, password=p, gpg_store=store
        )

    @mock.patch("sett.core.crypt.check_password")
    def test_check_password_matches_any_key_wrong_password(self, mock_check_password):
        mock_check_password.side_effect = UserError
        with self.assertRaises(UserError) as cm:
            crypt.check_password_matches_any_key("", [mock.Mock, mock.Mock], mock.Mock)
        self.assertEqual(
            format(cm.exception),
            "The provided password does not match any of the GPG keys with "
            "which the data was encrypted",
        )


class TestKeyDownlaod(unittest.TestCase):
    # pylint: disable=no-self-use
    def test_load_authority_key(self):
        # Check that passing a fingerprint runs the function, while passing
        # no fingerprint doesn't, thus running the function twice should
        # attempt to retrieve the key once.
        sett_config = Config()
        mock_retrieve_and_refresh_certification_authority_key = mock.Mock()
        with mock.patch(
            "libbiomedit.crypt.retrieve_and_refresh_certification_authority_key",
            mock_retrieve_and_refresh_certification_authority_key,
        ):
            for fingerprint in ("A" * 40, None):
                sett_config.key_authority_fingerprint = fingerprint
                crypt.load_authority_key(sett_config)

            mock_retrieve_and_refresh_certification_authority_key.assert_called_once()

        # Test that when offline or key download is disabled in the config,
        # the function is called with the correct value passed to allow_key_download.
        sett_config_1 = Config()
        sett_config_2 = Config()
        sett_config_2.offline = True
        sett_config_3 = Config()
        sett_config_3.gpg_key_autodownload = False
        for sett_config, expected_allow_key_download in (
            (sett_config_1, True),
            (sett_config_2, False),
            (sett_config_3, False),
        ):
            mock_retrieve_and_refresh_certification_authority_key = mock.Mock()
            mock_property_gpg_store = mock.Mock()
            with mock.patch(
                "libbiomedit.crypt.retrieve_and_refresh_certification_authority_key",
                mock_retrieve_and_refresh_certification_authority_key,
            ), mock.patch(
                "sett.utils.config.Config.gpg_store", mock_property_gpg_store
            ):
                crypt.load_authority_key(sett_config)
                mock_retrieve_and_refresh_certification_authority_key.assert_called_once_with(
                    key_fingerprint=sett_config.key_authority_fingerprint,
                    gpg_store=sett_config.gpg_store,
                    keyserver_url=sett_config.keyserver_url,
                    validate_key_origin=False,
                    allow_key_download=expected_allow_key_download,
                    url_opener=urlopen,
                )
