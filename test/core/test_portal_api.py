import json
import unittest
from io import StringIO
from unittest import mock
from urllib.error import HTTPError

from sett.core.error import UserError
from sett.core.metadata import MetaData
from sett.core.portal_api import PortalApi


class TestTransferVerification(unittest.TestCase):
    def test_verify_transfer(self):
        metadata = MetaData.from_dict(
            {
                "transfer_id": 42,
                "sender": "A" * 32,
                "recipients": ["B" * 256],
                "timestamp": "2019-10-11T14:50:12+0100",
                "checksum": "A" * 64,
                "checksum_algorithm": "SHA256",
                "compression_algorithm": "gzip",
                "version": MetaData.version,
                "purpose": "PRODUCTION",
            }
        )
        filename = "foo.tar"
        ref = json.dumps(
            {"file_name": filename, "metadata": json.dumps(MetaData.asdict(metadata))}
        ).encode()
        portal_api = PortalApi("https://portal.org")
        url = portal_api.base_url + PortalApi.dpkg_check
        with self.subTest("OK"):
            with mock.patch("sett.core.portal_api.post") as mock_post:
                mock_post.return_value = '{"project_code": 6}'
                portal_api.verify_transfer(metadata, filename)
                mock_post.assert_called_once_with(url, ref)
        with self.subTest("Error"):
            with mock.patch("sett.core.portal_api.post") as mock_post:
                mock_post.side_effect = HTTPError(
                    url=url,
                    code=400,
                    msg="Bad request",
                    hdrs={},
                    fp=StringIO('{"detail": "Invalid transfer"}'),
                )
                with self.assertRaises(UserError) as cm:
                    portal_api.verify_transfer(metadata, filename)
                self.assertEqual(format(cm.exception), "PortalApi: Invalid transfer")
